module nurdz.game
{
    /**
     * An object which maps digits into polygons that can be rendered for a simple numeric display.
     * The polygon data assumes that the top left of all of the character cells is 0,0 and that each level
     * is FONT_WIDTH x FONT_HEIGHT units in dimension.
     *
     * As such, you probably want to draw this scaled; note that when you scale the canvas, the location
     * of things rendered is scaled as well. For the purposes of our font, this works out OK.
     *
     * @type {Object<string,Polygon>}
     */
    var NUMBER_FONT = {
        "0": [['m', 0, 0], [3, 0], [3, 5], [0, 5], ['c'], ['m', 1, 1], [1, 4], [2, 4], [2, 1]],

        "1": [['m', 1, 0], [2, 0], [2, 5], [1, 5]],

        "2": [['m', 0, 0], [3, 0], [3, 3], [1, 3], [1, 4], [3, 4], [3, 5], [0, 5], [0, 2], [2, 2], [2, 1],
              [0, 1]],

        "3": [['m', 0, 0], [3, 0], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3], [0, 3], [0, 2], [2, 2], [2, 1],
              [0, 1]],

        "4": [['m', 0, 0], [1, 0], [1, 2], [2, 2], [2, 0], [3, 0], [3, 5], [2, 5], [2, 3], [0, 3]],

        "5": [['m', 0, 0], [3, 0], [3, 1], [1, 1], [1, 2], [3, 2], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3],
              [0, 3]],

        "6": [['m', 0, 0], [3, 0], [3, 1], [1, 1], [1, 2], [3, 2], [3, 5], [0, 5], ['c'], ['m', 1, 3], [1, 4],
              [2, 4], [2, 3]],

        "7": [['m', 0, 0], [3, 0], [3, 5], [2, 5], [2, 1], [1, 1], [1, 2], [0, 2]],

        "8": [['m', 0, 0], [3, 0], [3, 5], [0, 5], ['c'], ['m', 1, 1], [1, 2], [2, 2], [2, 1], ['c'],
              ['m', 1, 3],
              [1, 4], [2, 4], [2, 3]],

        "9": [['m', 0, 0], [3, 0], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3], [0, 3], ['m', 1, 1], [1, 2],
              [2, 2],
              [2, 1]]
    };

    /**
     * How many "units" wide the NUMBER_FONT font data points think that it is.
     *
     * @type {number}
     */
    const FONT_WIDTH = 3;

    /**
     * How many "units" tall the NUMBER_FONT font data points think that it is.
     * @type {number}
     */
    const FONT_HEIGHT = 5;

    /**
     * How many "units" should appear between consecutive NUMBER_FONT digits when they are rendered.
     *
     * @type {number}
     */
    const FONT_SPACING = 0.5;

    /**
     * This sets how big each unit in the font is when it is rendered. Thus each character in the font
     * will be FONT_WIDTH * FONT_SCALE pixels wide and FONT_HEIGHT * FONT_SCALE pixels tall. Set as
     * appropriate.
     *
     * Here we are saying that the overall height of a character is 64 pixels, so we need to divide that
     * by the height of the font in "font units".
     *
     * @type {number}
     */
    const FONT_SCALE = 64 / FONT_HEIGHT;

    /**
     * Crudely render a number using our number font.
     *
     * @param renderer the renderer to use to render the number
     * @param x the x position to render the top left of the number at
     * @param y the y position to render the top left of the number at
     * @param color the color to render the number
     * @param numString the string to render, which needs to be only digits.
     */
    export function renderNumber (renderer : CanvasRenderer, x : number, y : number, color : string,
                                  numString : string) : void
    {

        for (let i = 0 ; i < numString.length ; i++, x += (FONT_WIDTH * FONT_SCALE) + (FONT_SPACING * FONT_SCALE))
        {
            // Translate to where the number should start rendering, then scale the canvas. Since the font
            // data assumes 1 pixels per unit, the scale sets how many pixels wide each unit turns out.
            renderer.translateAndRotate (x, y);
            renderer.context.scale (FONT_SCALE, FONT_SCALE);

            var polygon = NUMBER_FONT[numString[i]];
            if (polygon)
                renderer.fillPolygon (polygon, color);

            renderer.restore ();
        }
    }

    /**
     * Given a string of digits, return back a point where the X value indicates how many pixels wide
     * and tall the rendered polygon for that text would be.
     *
     * @param numberStr the string to calculate the size of
     * @returns {Point} a point whose x value is the width of the string in pixels and whose y is the
     * height in pixels.
     */
    export function numberStringSize (numberStr : string) : Point
    {
        // Get the height and width of a digit in our number font in pixels based on the scale factor.
        let pixelWidth = FONT_WIDTH * FONT_SCALE;
        let pixelHeight = FONT_HEIGHT * FONT_SCALE;
        let pixelGap = FONT_SPACING * FONT_SCALE;

        return new Point (numberStr.length * pixelWidth + (numberStr.length - 1) * pixelGap,
            pixelHeight);
    }
}
