module nurdz.game
{
    /**
     * If the game remains on the title screen for this number of consecutive ticks, the game is
     * automatically kicked off with 0 players to start "attract mode".
     *
     * There are 30 ticks in a second because we run at 30fps, so there are 30 * 60 ticks per minute.
     *
     * @type {number}
     */
    export const ATTRACT_MODE_TICKS = (30 * 60) * 2;

    /**
     * This scene represents the title screen, where the game is described and the number of players is
     * selected.
     */
    export class TitleScene extends Scene
    {
        /**
         * This is true until the first time the scene is activated, then it becomes false; we use this to
         * determine if we should start our music playing or not.
         *
         * @type {boolean}
         * @private
         */
        private _firstActivation : boolean = true;

        /**
         * The music that plays on this scene; this is an object that is shared amongst all of the scenes
         * since they all play the same music.
         */
        private _music : Sound;

        /**
         * The image that we use to display our background.
         */
        private _background : HTMLImageElement;

        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * The number of ticks that we have waited without any interaction to select a number of players
         * by the user; this gets reset when we become active, and if it reached ATTRACT_MODE_TICKS, we
         * start a 0 player game.
         */
        private _attractTicks : number;

        /**
         * Construct a new title screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         * @param music the music to play (while music is not muted)
         */
        constructor (stage : Stage, music : Sound)
        {
            super ("titleScreen", stage);

            // Save the music.
            this._music = music;

            // Indicate that we want to load a background image. This is implicitly shared if another
            // scene loads the same image because image preloads are shared (music is not).
            this._background = stage.preloadImage ("background.png");
        }

        /**
         * This gets invoked when our scene becomes the active scene; If the music is not muted and not
         * already playing, this starts it.
         *
         * @param previousScene the scene that used to be active.
         */
        activating (previousScene : nurdz.game.Scene) : void
        {
            // Let the super do its thing.
            super.activating (previousScene);

            // Reset our attract mode counter.
            this._attractTicks = 0;

            // If we have music and this is the first time we've been activated, start the music playing;
            // we don't want to play the music at other times in case it's not playing because the user
            // muted it.
            if (this._music && this._firstActivation)
                this._music.play (false);

            // Clear the flag now
            this._firstActivation = false;
        }

        /**
         * Invoked every time a key is pressed on the title screen.
         *
         * @param eventObj the keyboard event that says what key was pressed
         * @returns {boolean} true if we handle the key, false otherwise.
         */
        inputKeyDown (eventObj : KeyboardEvent) : boolean
        {
            // If the super handles the key, return true. This lets default handling for known keys do its
            // thing.
            if (super.inputKeyDown (eventObj))
                return true;

            switch (eventObj.keyCode)
            {
                // Toggle the mute state of the music
                case KeyCodes.KEY_SPACEBAR:
                case KeyCodes.KEY_M:
                    if (this._music)
                        this._music.toggle (false);
                    return true;

                // Select a single or two player game; you can also force attract mode by pressing 0,
                // although this is not documented. Easter Egg!
                case KeyCodes.KEY_0:
                case KeyCodes.KEY_1:
                case KeyCodes.KEY_2:
                    playerCount = eventObj.keyCode - KeyCodes.KEY_0;
                    this._stage.switchToScene ("game");
                    return true;

                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;
            }

            return false;
        }

        /**
         * Invoked every update.
         * @param tick
         */
        update (tick : number) : void
        {
            // Let the super do its thing.
            super.update (tick);

            // Update our tick count internally. If it reaches the appropriate point, start the game with
            // 0 players.
            this._attractTicks++;
            if (this._attractTicks >= ATTRACT_MODE_TICKS)
            {
                playerCount = 0;
                this._stage.switchToScene ("game");
            }
        }

        /**
         * This is called to invoke our
         */
        render () : void
        {
            // Render our background. This should cover the whole canvas, so there is no need to do any
            // clearing.
            this._renderer.blit (this._background, 0, 0);

            // Save the current context state.
            this._renderer.translateAndRotate (null, null, null);

            // Title text.
            this._renderer.context.font = "64px monospace";
            this._renderer.context.textAlign = "center";
            this._renderer.drawTxt ("Tennis Mania", this._renderer.width / 2, 64, 'green');

            // Rule text.
            this._renderer.context.font = "25px monospace";
            this._renderer.context.textAlign = "left";

            this._renderer.drawTxt ("First one to " + WINNING_SCORE + " points wins!", 20, 150, 'white');
            this._renderer.drawTxt ("The other player is the big L.O.S.E.R!", 35, 180, 'red');

            // Controls
            this._renderer.drawTxt ("Single player controls:", 20, 250, 'white');
            this._renderer.drawTxt ("mouse or the 'w' and 's' keys", 80, 280, 'darkred');

            this._renderer.drawTxt ("Two player controls:", 20, 350, 'white');
            this._renderer.drawTxt ("player 1 -> 'w' and 's' keys", 80, 380, 'darkred');
            this._renderer.drawTxt ("player 2 -> arrow keys", 80, 410, 'darkred');

            this._renderer.drawTxt("Press 'm' or space bar to toggle music", 20, 480, 'darkred');
            this._renderer.drawTxt("Press 'f' to toggle full screen", 20, 510, 'darkred');

            // Player select instructions
            this._renderer.context.textAlign = 'center';
            this._renderer.drawTxt ("Press '1' or '2' to select the number of players",
                                    this._renderer.width / 2, this._renderer.height - 25, 'white');

            // Restore the context and let the super render things now.
            this._renderer.restore ();
            super.render ();
        }
    }
}
