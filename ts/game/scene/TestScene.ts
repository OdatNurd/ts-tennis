module nurdz.game
{
    /**
     * This scene represents a simple testing scene, where testing is tested for test purposes.
     */
    export class TestScene extends Scene
    {
        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * The ball entity.
         */
        protected _ball : Entity;

        /**
         * The speed that the ball travels.
         */
        protected _speed : Vector2D;

        /**
         * A circular collider whose position represents the end of the speed vector for the ball; used to
         * manipulate it.
         */
        protected _velEndPos : Collider;

        /**
         * True if the ball should be moving, or false if it should be stationary. Toggled with the space bar.
         */
        protected _moveBall : boolean;

        /**
         * This is either null when no dragging is going on or one of the draggable colliders otherwise.
         */
        private _draggedControl : Collider;

        /**
         * A fake paddle that we stick into the scene to test for pushing out of collisions.
         */
        private _paddle : Entity;

        /**
         * Construct a test scene.
         *
         * @param stage the stage that will control this animation
         */
        constructor (stage : Stage)
        {
            super ("testScene", stage);

            // Create our entities now. The ball will start centered on the screen and be a circle with a
            // radius of 20 with the origin at its center.
            this._ball = new Entity ("Ball", stage, (stage.width / 2) - 300, (stage.height / 2) - 200,
                40, 40, 1, {});
            this._ball.makeCircle (20, true);

            // The paddle is a simple rectangle off to the side.
            this._paddle = new Entity ("Paddle", stage, stage.width / 2, stage.height / 2, 50, 250, 1, {});
            this._paddle.origin.setToXY (this._paddle.width / 2, this._paddle.height / 2);

            // Set the initial speed vector for the ball.
            this._speed = new Vector2D (64, 64);

            // Create the endpoint for the velocity vector.
            this._velEndPos = new Collider (stage, ColliderType.CIRCLE,
                this._ball.position.x + this._speed.x,
                this._ball.position.y + this._speed.y,
                10);

            // Add the entities to the stage so that they update and render.
            this.addActor (this._ball);
            this.addActor (this._paddle);

            // Ball does not move by default.
            this._moveBall = false;
        }

        /**
         * Invoked every time a key is pressed on the game screen.
         *
         * @param eventObj the keyboard event that says what key was pressed
         * @returns {boolean} true if we handle the key, false otherwise.
         */
        inputKeyDown (eventObj : KeyboardEvent) : boolean
        {
            // If the super handles the key, we're done
            if (super.inputKeyDown (eventObj))
                return true;

            // See if we care.
            switch (eventObj.keyCode)
            {
                // Space toggles the ball motion.
                case KeyCodes.KEY_SPACEBAR:
                    this._moveBall = !this._moveBall;
                    return true;

                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;
            }

            return false;
        }

        /**
         * Triggers when the mouse is pressed down while on the stage
         *
         * @param eventObj the event that tracks the mouse position
         * @returns {boolean} true always to keep the browser from taking liberties
         */
        inputMouseDown (eventObj : MouseEvent) : boolean
        {
            // If the ball is moving, we do nothing.
            if (this._moveBall)
                return true;

            // Get the position of the mouse at this location.
            let mousePos = this._stage.calculateMousePos (eventObj);

            // See if the mouse position is inside of one of the two controls.
            if (this._ball.contains (mousePos))
                this._draggedControl = this._ball;

            else if (this._velEndPos.contains (mousePos))
                this._draggedControl = this._velEndPos;

            else
                this._draggedControl = null;

            // Assume we handled this; it stops the browser from doing its own drag handling.
            return true;
        }

        /**
         * Triggers when the mouse is released; this happens everywhere, not just over the stage.
         *
         * @param eventObj the event that tracks the mouse position
         * @returns {boolean} true if we handle the event, false otherwise
         */
        inputMouseUp (eventObj : MouseEvent) : boolean
        {
            // Set the dragged control to null to end the drag; if there isn't one, this does nothing.
            this._draggedControl = null;
            return true;
        }

        /**
         * When the mouse moves, magic happens.
         *
         * @param eventObj the mouse event object
         * @returns {boolean} whether or not we handled it
         */
        inputMouseMove (eventObj : MouseEvent) : boolean
        {
            // If there is no dragged control, do nothing.
            if (this._draggedControl == null)
                return true;

            // Get the position of the mouse at this location.
            let mousePos = this._stage.calculateMousePos (eventObj);

            // Set the location of our dragged control.
            this._draggedControl.position.setTo (mousePos);

            // If the position dragged was the speed control, we need to update the speed vector now.
            if (this._draggedControl === this._velEndPos)
                this._speed = Vector2D.fromPoint (this._draggedControl.position, this._ball.position);

            // Update the velocity end control so that it renders in the appropriate location.
            this._velEndPos.position.setToXY (
                this._ball.position.x + this._speed.x,
                this._ball.position.y + this._speed.y
            );

            return true;
        }

        /**
         * In response to a collision originating at fromPosition, push object backwards along the motion
         * vector given until it has moved a total of distance units along the X or Y axis.
         *
         * This calculates the distance backwards along the motion vector needed to put the object at a
         * position where it has moved exactly the provided distance along either the X or Y axis.
         *
         * @param object the object to push
         * @param motion vector that describes the motion path of the object
         * @param fromPosition the position to base the push on
         * @param distance the distance along the X or Y axis the ball needs to ultimately move.
         * @param xAxis true if the distance given is on the X axis or false if it is on the Y axis.
         */
        pushOut (object : Entity, motion : Vector2D, fromPosition : Point, distance : number,
                 xAxis : boolean) : void
        {
            console.log (`Push out ${distance} along xAxis: ${xAxis}`);

            // Make a copy of the motion vector and reverse it so we can follow back along the path
            // the object used to get here.
            //
            // We use some algebra and trig to determine how far back along this reverse vector we need to
            // go in order to get an axial displacement like the one given.
            let revSpeed = motion.copyReversed ();

            console.log ("xAxis:", Math.abs (distance / Math.cos (revSpeed.direction)));
            console.log ("yAxis:", Math.abs (distance / Math.sin (revSpeed.direction)));

            // Calculate based on the axis; note that this works around a bug in the magnitude code because
            // some angles provide a negative sin/cos, which implicitly re-reverses the vector, which is not
            // what we want.
            if (xAxis)
                revSpeed.magnitude = Math.abs (distance / Math.cos (revSpeed.direction));
            else
                revSpeed.magnitude = Math.abs (distance / Math.sin (revSpeed.direction));

            // Put the object at the position provided, then translate backwards.
            object.position.setTo (fromPosition);
            object.position.translate (revSpeed);
        }

        /**
         * Check to see if the ball has collided with any of the collision objects. If it has, the exact
         * location of the collision on the bounds of the object, as well as info on whether it was on a
         * vertical or horizontal segment is returned back, and the ball is shifted to the position before
         * the impact occured.
         *
         * @returns {BallCollision} null if no collision or info on the collision otherwise.
         */
        private checkColliders () : BallCollision
        {
            // Iterate over all of our colliders. The first one we collide with, return its Y position.
            for (let i = 0 ; i < 1 ; i++)
            {
                // Alias the ball; this would be "this" in the actual code.
                let ball = this._ball;

                // Alias the collider to make the code below more readable (and maybe faster because no
                // array access? I dunno).
                let other = this._paddle;

                // We get called after the ball has already moved. Our first check is, "was the position
                // that the ball was in at the start of the frame inside the paddle?"
                //
                // If it is, then any intersection we're about to get is going to be as the ball exits the
                // paddle, and we want to ignore that. This can happen if the paddle moves after the ball
                // moves in the last frame and puts the ball inside of itself. This either means that the
                // paddle moved to block the ball too late OR that the collision was on the top/bottom of
                // the paddle, in which case the paddle moved onto it after it turned around.
                if (other.containsXY (ball.position.x - this._speed.x, ball.position.y - this._speed.y))
                    continue;

                // The ball wasn't inside the paddle when it started, so see if it ends up inside the paddle
                // or passes through it by checking the line segment representing its motion against this
                // collision object.
                //
                // The directionality of the speed vector inherently allows the collision code to determine
                // where the collision happened.
                let intersect = other.intersectWithSegmentXY (ball.position.x - this._speed.x,
                                                              ball.position.y - this._speed.y,
                                                              ball.position.x, ball.position.y);

                // If we did intersect, then we need to do some work.
                if (intersect != null)
                {
                    // Get the origin-corrected bounding position of the collision object that we hit.
                    let left = other.position.x - other.origin.x;
                    let top = other.position.y - other.origin.y;
                    let right = left + other.width;
                    let bottom = top + other.height;

                    // Create our eventual return object. This contains the exact position that the
                    // intersection happened at as well as an indication of whether this was a hit on a
                    // vertical edge (thus a horizontal reflection) or a horizontal edge (thus a vertical
                    // reflection).
                    //
                    // To determine that, we know that the collision code will always return an intersect
                    // that has at least one value that is exactly on an edge; it could also hit the
                    // corner exactly, but in that case we'll treat it as a horizontal reflection.
                    let retVal : BallCollision = {
                        other:      other,
                        position:   intersect,
                        horizontal: intersect.x == left || intersect.x == right
                    };

                    // Push back our radius on the axis that we collided on
                    this.pushOut (ball, this._speed, intersect, ball.radius, retVal.horizontal);

                    // Stop the ball so we can see where we ended up.
                    this._moveBall = false;

                    // Return our result now.
                    return retVal;
                }
            }

            // Not colliding with anything.
            return null;
        }

        /**
         * Given some value and a range, return back exactly how far out of range the number is.
         *
         * The return value is 0 if the number is within range.
         *
         * @param value the value to test
         * @param min the minimum value
         * @param max the maximum value
         */
        private rangeOverflow (value : number, min : number, max : number) : number
        {
            // Too small?
            if (value < min)
                return min - value;

            // Too large?
            if (value > max)
                return value - max;

            // In range.
            return 0;
        }

        /**
         * Check to see if the ball is colliding, and update its position and velocity as appropriate.
         */
        collide () : void
        {
            // Check to see if the ball is colliding with an object.
            let collideInfo = this.checkColliders ();
            if (collideInfo != null)
            {
                // Based on the reflection needed, modify the speed.
                if (collideInfo.horizontal == true)
                    this._speed.flipX ();
                else
                    this._speed.flipY ();
            }

            // The code here uses some algebra and trig to determine how far along a reversed speed vector
            // (the hypotenuse of a right triangle) we need to travel to get the right amount of X or Y
            // displacement to make our edge appear to rest on the screen edge.
            //
            // To do this we calculate how far away from a constrained edge we are. Basically we stop within
            // our radius' value of the edges because our position is determined from our center point. When
            // we are calculating the displacement, we specify how far outside of that constrained range
            // we are.
            //
            // You might think that we need to include our radius in the calculation, but we don't, because
            // it's already included in the bounding rectangle we use to determine the collision.

            // First, check to see if we're off the top or bottom of the screen.
            let over : number = this.rangeOverflow (this._ball.position.y,
                                                    this._ball.radius,
                                                    this._stage.height - this._ball.radius);
            if (over != 0)
            {
                // Push back the given distance on the Y axis.
                this.pushOut (this._ball, this._speed, this._ball.position, over, false);

                // Now reverse the Y speed to go back the other way.
                this._speed.flipY ();
            }

            // Now see if we're off the left or right side of the screen.
            over = this.rangeOverflow (this._ball.position.x,
                                       this._ball.radius,
                                       this._stage.width - this._ball.radius - 1);
            if (over != 0)
            {
                // Push back the given distance on the X axis.
                this.pushOut (this._ball, this._speed, this._ball.position, over, true);

                // Go back the other way.
                this._speed.flipX ();
            }
        }

        /**
         * Invoked once per game tick to update the frame and its contents.
         *
         * @param tick the current tick; increases by one for each invocation
         */
        update (tick : number) : void
        {
            // Is the ball supposed to move?
            if (this._moveBall)
            {
                // Make the ball follow it's velocity vector
                this._ball.position.translate (this._speed);

                // Check collisions.
                this.collide ();

                // Lastly, update the velocity end position control so that it renders in the appropriate new
                // location. This has to happen after the collision code so that the velocity vector changes
                // (if any) are taken into account.
                this._velEndPos.position.setToXY (
                    this._ball.position.x + this._speed.x,
                    this._ball.position.y + this._speed.y
                );
            }
        }

        /**
         * This is called whenever our scene needs to be rendered.
         */
        render () : void
        {
            // Clear
            this._renderer.fillRect (0, 0, this._stage.width, this._stage.height, 'black');

            // Let the super render things now. This will render our entities
            super.render ();

            // Render the velocity arrow and it's control endpoint.
            this._renderer.strokeCircle (this._velEndPos.position.x, this._velEndPos.position.y,
                                         this._velEndPos.radius, 'red', 2);
            this._renderer.setArrowStyle ('green', 3);
            this._renderer.drawArrow (this._ball.position.x, this._ball.position.y,
                                      this._ball.position.x + this._speed.x,
                                      this._ball.position.y + this._speed.y,
                                      ArrowStyle.ARC, ArrowType.END);

        }
    }
}
