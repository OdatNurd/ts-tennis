module nurdz.game
{
    /**
     * This scene represents the game screen, where the game is actually played.
     */
    export class GameScene extends Scene
    {
        /**
         * The music that plays on this scene; this is an object that is shared amongst all of the scenes
         * since they all play the same music.
         */
        private _music : Sound;

        /**
         * The image that we use to display our background.
         */
        private _background : HTMLImageElement;

        /**
         * The list of colors that we use to display our attract mode text if we are in attract mode (0
         * players).
         * @type {Array<string>}
         * @private
         */
        private _attractColors : Array<string> = ['#ffffff', '#000000'];

        /**
         * The index into _attractColors that we use to display attract mode text (if we're doing that).
         *
         * @type {number}
         * @private
         */
        private _colorIndex : number = 0;

        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * The ball entity.
         */
        protected _ball : Ball;

        /**
         * The left paddle.
         */
        protected _paddleLeft : Paddle;

        /**
         * The right paddle.
         */
        protected _paddleRight : Paddle;

        /**
         * The direction that the left paddle should move in reaction to keyboard input. A value of 0
         * means stationary, < 0 means up and > 0 means down.
         *
         * The value is multiplied by PADDLE_KEYBOARD_SPEED to obtain the amount to move, so this can be
         * used to ramp up speed or what have you.
         */
        protected _leftControl : number;

        /**
         * The direction that the right paddle should move in reaction to keyboard input. A value of 0
         * means stationary, < 0 means up and > 0 means down.
         *
         * The value is multiplied by PADDLE_KEYBOARD_SPEED to obtain the amount to move, so this can be
         * used to ramp up speed or what have you.
         *
         * This value is only non-zero when a two player game is in effect; otherwise the paddle moves itself.
         */
        protected _rightControl : number;

        /**
         * The sound that plays when someone scores.
         */
        private _sndScore : Sound;

        /**
         * The sound that plays when the ball reflects from a paddle.
         */
        private _sndBouncePaddle : Sound;

        /**
         * The sound that plays when the ball bounces off of a wall (top or bottom of the screen).
         */
        private _sndBounceWall : Sound;

        /**
         * Construct a new game screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         * @param music the music to play (while music is not muted)
         */
        constructor (stage : Stage, music : Sound)
        {
            super ("gameScreen", stage);

            // Save the music.
            this._music = music;

            // Preload all of the sounds we use. We load our own sounds; the music is given to us because
            // its shared amongst all scenes.
            this._sndScore = stage.preloadSound ("score");
            this._sndBouncePaddle = stage.preloadSound ("bounce_paddle");
            this._sndBounceWall = stage.preloadSound ("bounce_wall");

            // Indicate that we want to load a background image. This is implicitly shared if another
            // scene loads the same image because image preloads are shared (music is not).
            this._background = stage.preloadImage ("background.png");

            // Create our entities now.
            this._ball = new Ball (stage, this);
            this._paddleLeft = new Paddle (stage, PADDLE_EDGESPACE, "paddle_2_1.png", 0);
            this._paddleRight = new Paddle (stage, stage.width - PADDLE_EDGESPACE, "paddle_2_1.png", 1);

            // // Turn on debug mode for all of the entities so that we can verify that everything is working
            // // the way we want it to.
            // this._ball.properties.debug = true;
            // this._paddleLeft.properties.debug = true;
            // this._paddleRight.properties.debug = true;

            // Register the paddle objects with the ball; these are the objects that it can collide with
            // to change directions.
            this._ball.addCollider (this._paddleLeft);
            this._ball.addCollider (this._paddleRight);

            // Add all entities as actors so that they get updated and rendered. Note that we add the ball
            // first; that means it gets updates and renders first, which means that it renders below the
            // paddles, which is a nice visual tweak to hide our less than perfect collision routines.
            this.addActor (this._ball);
            this.addActor (this._paddleLeft);
            this.addActor (this._paddleRight);

            // By default there is no motion.
            this._leftControl = 0;
            this._rightControl = 0;
        }

        /**
         * This gets invoked when our scene becomes the active scene; If the music is not muted and not
         * already playing, this starts it.
         *
         * @param previousScene the scene that used to be active.
         */
        activating (previousScene : nurdz.game.Scene) : void
        {
            // Let the super do its thing.
            super.activating (previousScene);

            // This really only has to happen once, but make sure that we know the sizes of the paddles.
            setPaddleWidths (this._stage, this._paddleLeft.width, this._paddleRight.width);

            // Reset the scores back to zero, and get the ball ready.
            resetScores ();
            this._ball.reset ();

            // We are becoming the active scene; Tell our paddles to track the ball (or not) depending on
            // the number of players. The left paddle only tracks the ball when nobody is playing, and the
            // right paddle only tracks the ball when a second person is not playing.
            this._paddleLeft.track (playerCount == 0 ? this._ball : null);
            this._paddleRight.track (playerCount != 2 ? this._ball : null);
        }

        /**
         * This checks the key code provided to if it is one of the keys that handles controlling the
         * paddles in the game, and takes appropriate action.
         *
         * The boolean parameter pressed tells us if the button is currently pressed (true) or released
         * (false), so that we know how to move the appropriate paddle.
         *
         * This takes care to make sure that two player controls control the first player when in single
         * player mode.
         *
         * @param eventObj the event object that
         * @param pressed true if the event indicates the button is pressed or false if it was released
         * @returns {boolean} true if the event was handled or false otherwise.
         */
        private handlePaddleKey (eventObj : KeyboardEvent, pressed : boolean) : boolean
        {
            // Handle based on the key code.
            switch (eventObj.keyCode)
            {
                // Right player controls move the right paddle in a two player game and the left paddle in a
                // single player game.
                case KeyCodes.KEY_UP:
                    if (playerCount == 1)
                        this._leftControl = (pressed ? -1 : 0);
                    else if (playerCount == 2)
                        this._rightControl = (pressed ? -1 : 0);
                    return true;

                case KeyCodes.KEY_DOWN:
                    if (playerCount == 1)
                        this._leftControl = (pressed ? 1 : 0);
                    else if (playerCount == 2)
                        this._rightControl = (pressed ? 1 : 0);
                    return true;

                // Left player keys only ever control the left paddle no matter what, if there is at least
                // one player.
                case KeyCodes.KEY_W:
                    if (playerCount != 0)
                        this._leftControl = (pressed ? -1 : 0);
                    return true;

                case KeyCodes.KEY_S:
                    if (playerCount != 0)
                        this._leftControl = (pressed ? 1 : 0);
                    return true;

                // We don't handle anything else.
                default:
                    return false;
            }
        }

        /**
         * Invoked every time a key is pressed on the game screen.
         *
         * @param eventObj the keyboard event that says what key was pressed
         * @returns {boolean} true if we handle the key, false otherwise.
         */
        inputKeyDown (eventObj : KeyboardEvent) : boolean
        {
            // If the number of players is 0, ignore everything else and immediately switch back to the
            // title screen scene.
            if (playerCount == 0)
            {
                this._stage.switchToScene ("title");
                return true;
            }

            // If the super handles the key or if it is a paddle key, return true.
            if (super.inputKeyDown (eventObj) || this.handlePaddleKey (eventObj, true))
                return true;

            // See if it's something else we care about.
            switch (eventObj.keyCode)
            {
                // Toggle the mute state of the music
                case KeyCodes.KEY_SPACEBAR:
                case KeyCodes.KEY_M:
                    if (this._music)
                        this._music.toggle (false);
                    return true;

                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;
            }

            return false;
        }

        /**
         * Invoked every time a key is released on the game screen.
         *
         * @param eventObj the keyboard event that says what key was released
         * @returns {boolean} true if we handle the key, false otherwise.
         */
        inputKeyUp (eventObj : KeyboardEvent) : boolean
        {
            // Check if this is a paddle key being released.
            if (this.handlePaddleKey (eventObj, false))
                return true;

            // Let the super do its thing.
            return super.inputKeyUp (eventObj);
        }

        /**
         * Handle a mouse click on the canvas while the game is in play.
         *
         * @param eventObj the mouse event that represents the click
         * @returns {boolean} true if we handle the click, false otherwise.
         */
        inputMouseClick (eventObj : MouseEvent) : boolean
        {
            // If this is a 0 player game, switch back to the title screen.
            if (playerCount == 0)
            {
                this._stage.switchToScene ("title");
                return true;
            }

            // Do what the super does.
            return super.inputMouseClick (eventObj);
        }

        /**
         * This is triggered whenever the mouse is moved over the canvas. In a single player game, this
         * controls where the left paddle is located.
         *
         * @param eventObj the event that represents the mouse movement.
         * @returns {boolean} true if we handled this event or false if not.
         */
        inputMouseMove (eventObj : MouseEvent) : boolean
        {
            // If we're in single player mode, we want to read the mouse position and use the vertical
            // component to set the position of the left paddle.
            if (playerCount == 1)
            {
                // Get the mouse position and use it to set the vertical position of the left paddle.
                this._paddleLeft.jumpTo (this._stage.calculateMousePos (eventObj).y);
                return true;
            }

            return false;
        }

        /**
         * This helper tells both of the paddles to update their tracking position based on the current
         * location and momentum of the ball.
         *
         * Any paddle that is not currently tracking the ball will ignore this call.
         */
        private updatePaddleTracking () : void
        {
            this._paddleLeft.updateTracking ();
            this._paddleRight.updateTracking ();
        }

        /**
         * This gets invoked every time the ball gets bounced off of a wall in the game in a non-scoring
         * situation.
         */
        wallBounce () : void
        {
            // Play the sound for the ball bouncing off of the wall.
            this._sndBounceWall.play ();
        }

        /**
         * This gets invoked every time the ball gets reflected from one side of the screen to the other
         * by bouncing off a paddle (or other collider).
         *
         * This happens after the reflection has happened.
         */
        reflect () : void
        {
            // Play the paddle collision sound.
            this._sndBouncePaddle.play ();

            // Tell the paddles that the ball has reflected so that they can update their positions.
            this.updatePaddleTracking ();
        }

        /**
         * This gets invoked every time the ball reaches the right or left side of the screen.
         *
         * The ball invokes this when it discovers that it has gone off the screen, allowing us to handle
         * it as appropriate.
         */
        score () : void
        {
            // Determine the score that will end the game.
            let winningScore = (playerCount == 0 ? ATTRACT_SCORE : WINNING_SCORE);

            // Update the score of the appropriate player based on the position of the ball. This has to
            // be first, since we're about to fiddle the ball position.
            if (this._ball.position.x <= 0)
                player2Score++;
            else
                player1Score++;

            // If either score is a winning score, switch to the appropriate scene.
            //
            // In "Attract mode" (0 players) we go back to the title screen instead of saying the score at
            // the win screen.
            if (player1Score >= winningScore || player2Score >= winningScore)
                this._stage.switchToScene (playerCount != 0 ? "win" : "title");

            // Play the score sound
            this._sndScore.play ();

            // Reset the position of the ball and then get both of the paddles to update their tracking
            // based on the new position.
            this._ball.reset ();
            this.updatePaddleTracking ();
        }

        /**
         * Invoked once per game tick to update the frame and its contents.
         *
         * @param tick the current tick; increases by one for each invocation
         */
        update (tick : number) : void
        {
            // Update the attract mode text color.
            if (playerCount == 0 && tick % 7 == 0)
            {
                this._colorIndex++;
                if (this._colorIndex == this._attractColors.length)
                    this._colorIndex = 0;
            }

            // Use the control values to shift the locations of the paddles. This happens in response to
            // the keyboard controls; the mouse
            if (this._leftControl != 0)
                this._paddleLeft.move (this._leftControl * PADDLE_KEYBOARD_SPEED);

            if (this._rightControl != 0)
                this._paddleRight.move (this._rightControl * PADDLE_KEYBOARD_SPEED);

            // let the super update; this calls update on our entities, which means that the ball will
            // move and the computer controlled paddle will have a chance to move itself.
            super.update (tick);
        }

        /**
         * This renders some attract mode text; it should really be invoked only when
         */
        private renderAttractMode () : void
        {
            // Save the current render context so we can fiddle with it
            this._renderer.translateAndRotate (null, null, null);

            // Set up our font and render some text.
            this._renderer.context.font = "25px monospace";
            this._renderer.context.textAlign = "center";
            this._renderer.context.textBaseline = "middle";
            this._renderer.drawTxt ("Press any key to play!", this._stage.width / 2, this._stage.height / 2,
                                    this._attractColors[this._colorIndex]);

            // Restore the context now
            this._renderer.restore ();
        }

        /**
         * This is called whenever our scene needs to be rendered.
         */
        render () : void
        {
            // Render our background. This should cover the whole canvas, so there is no need to do any
            // clearing.
            this._renderer.blit (this._background, 0, 0);

            // Let the super render things now. This will render the paddle and the ball for us.
            super.render ();

            // If there are 0 players, render our attract mode text.
            if (playerCount == 0)
                this.renderAttractMode ();

            // Now render the scores last, so that they appear on top of everything else.
            renderScores (this._renderer);
        }
    }
}
