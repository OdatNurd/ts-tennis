module nurdz.game
{
    /**
     * This is the scene that is displayed when someone wins the game. This can be either the/a player or
     * the computer, depending on what kind of game we have.
     */
    export class WinScene extends Scene
    {
        /**
         * The music that plays on this scene; this is an object that is shared amongst all of the scenes
         * since they all play the same music.
         */
        private _music : Sound;

        /**
         * The image that we use to display our background.
         */
        private _background : HTMLImageElement;

        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * A point that represents the center of the stage, which we use for positioning things when we
         * render.
         */
        private _center : Point;

        /**
         * Construct a new win screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         * @param music the music to play (while music is not muted)
         */
        constructor (stage : Stage, music : Sound)
        {
            super ("winScreen", stage);

            // Save the music.
            this._music = music;

            // Indicate that we want to load a background image. This is implicitly shared if another
            // scene loads the same image because image preloads are shared (music is not).
            this._background = stage.preloadImage ("background.png");

            // Save the center of the stage.
            this._center = new Point (stage.width / 2, stage.height / 2);
        }

        /**
         * Invoked every time a key is pressed on the win screen.
         *
         * @param eventObj the keyboard event that says what key was pressed
         * @returns {boolean} true if we handle the key, false otherwise.
         */
        inputKeyDown (eventObj : KeyboardEvent) : boolean
        {
            // If the super handles the key, return true. This lets default handling for known keys do its
            // thing.
            if (super.inputKeyDown (eventObj))
                return true;

            switch (eventObj.keyCode)
            {
                // Toggle the mute state of the music
                case KeyCodes.KEY_SPACEBAR:
                case KeyCodes.KEY_M:
                    if (this._music)
                        this._music.toggle (false);
                    return true;

                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;

                // Everything else starts a new game by going to the title screen.
                default:
                    this._stage.switchToScene ("title");
                    return true;
            }
        }

        /**
         * Invoked every time the mouse is clicked on the win screen.
         *
         * @param eventObj the mouse event that gives click details
         * @returns {boolean} true if we handle the click, false otherwise.
         */
        inputMouseClick (eventObj : MouseEvent) : boolean
        {
            // Start a new game by going to the title screen
            this._stage.switchToScene ("title");
            return true;
        }

        /**
         * This is called to invoke our
         */
        render () : void
        {
            // Render our background. This should cover the whole canvas, so there is no need to do any
            // clearing.
            this._renderer.blit (this._background, 0, 0);

            // Now the game scores.
            renderScores (this._renderer);

            // Determine the message that we will display. This depends on who won and how many players
            // there are.
            let message;
            if (playerCount == 1)
                message = (player1Score >= WINNING_SCORE ? "You Win!" : "Computer Wins!");
            else
                message = (player1Score >= WINNING_SCORE ? "Player 1 wins!" : "Player 2 wins!");

            // Save the current context state.
            this._renderer.translateAndRotate (null, null, null);

            // Win message
            this._renderer.context.font = "64px monospace";
            this._renderer.context.textAlign = "center";
            this._renderer.drawTxt (message, this._center.x, this._center.y, 'white');

            // The message for how to restart the game
            this._renderer.context.font = "25px monospace";
            this._renderer.drawTxt ("Click or press any key to play again",
                                    this._center.x, this._center.y + 80, 'white');

            // Restore the context and let the super render things now.
            this._renderer.restore ();
            super.render ();
        }

    }
}
