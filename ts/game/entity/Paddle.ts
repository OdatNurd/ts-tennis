module nurdz.game
{
    /**
     * The distance from the edge of the screen that each of the paddles is rendered. Larger values mean
     * farther from the edge of the screen and thus less reaction time.
     *
     * @type {number}
     */
    export const PADDLE_EDGESPACE = 50;

    /**
     * The speed that the keyboard moves the paddle if it's used as the control. THis applies to both the
     * left and right paddles equally/
     *
     * @type {number}
     */
    export const PADDLE_KEYBOARD_SPEED = 10;

    /**
     * The speed that the computer moves the paddle when it is in control (i.e. in a single player gme).
     * A higher value makes the paddle move faster, which makes the computer harder to beat.
     *
     * @type {number}
     */
    export const PADDLE_COMPUTER_SPEED = 10;

    /**
     * When a paddle is controlled via the computer and has calculated its final target position, the
     * target is modified by this value up or down from the calculated intersection point.
     *
     * This represents the percentage of the height of the paddle the target location can be skewed by at
     * most, either up or down.
     *
     * TODO This value could be modified to set difficulty
     *
     * @type {number}
     */
    export const PADDLE_MISS_FUDGE = 0.75;

    /**
     * This is used in the calculations that alter the vertical speed of the ball when interacting with a
     * paddle.
     *
     * The collision code calculates the difference in vertical position between the ball and the collided
     * object and uses that as a base speed, which is multiplied by this value to tone it down a little
     * bit so as to not allow the ball to get too wild.
     *
     * @type {number}
     */
    export const PADDLE_EDGESPEED_DAMPER = 0.40;

    /**
     * This sets the limit for how far away from the edges of the screen the vertical position of the
     * paddle can get before it is no longer allowed to move in that direction. A value of 0 means that
     * the center of the paddle will stop right at the extreme limits of the screen edges.
     *
     * The value here mimics older pong games that stop the paddles before they get to the edges, to
     * provide for a little extra "strategy".
     * @type {number}
     */
    export const PADDLE_EDGE_MOVE_LIMIT = 90;

    /**
     * The paddle entity is the entity that controls the paddle used by the player(s). Also included is an
     * AI module that will allow it to attempt to track the ball in order to return a serve.
     */
    export class Paddle extends Entity
    {
        /**
         * The ball in the game. This is only set when this paddle should be controlled by the computer AI
         * and not by human controls.
         *
         * Essentially, when this is non-null, the update() method uses the information in the ball to
         * determine where to move itself.
         */
        private _ball : Ball;

        /**
         * When this paddle is controlled by AI (_ball is non-null), this is the Y value on the screen
         * that this paddle is aiming for. As the ball is approaching the paddle, this is the location of
         * the expected interception point of the ball, and when it is going away, this is the center of
         * the field (to make a return volley easier next time).
         */
        private _targetY : number;

        /**
         * Construct a new paddle handled by the provided stage. The paddle is always initially vertically
         * centered on the stage, but its horizontal position needs to be provided as it can be on either
         * the left or right side of the screen.
         *
         * The image string provided is the filename of the image to use as the sprite sheet for this
         * paddle. The sprite number provided will be the one used to render this paddle.
         *
         * @param stage the stage to render on
         * @param x initial x position of the center of the paddle
         * @param image the filename of the image to use to render this paddle; should be a sprite sheet image
         * @param sprite the sprite number in the sprite image to use for this paddle.
         */
        constructor (stage : Stage, x : number, image : string, sprite : number)
        {
            // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
            // of ourselves. We always start with our position centered vertically, but we need to be told
            // our horizontal position.
            super ("paddle", stage, x, stage.height / 2, 0, 0, 1, {}, {}, 'red');

            // Create our sprite sheet for the paddle; setDimensions() will be called to finish our
            // initialization.
            this._sheet = new SpriteSheet (stage, image, 2, 1, true, this.setDimensions);
            this._sprite = sprite;

            // The target location defaults to our current location, so we don't move (if we're tracking
            // anything).
            this._targetY = this._position.y;
        }

        /**
         * Set the dimensions of the entity based on the size of the sprites in the SpriteSheet provided
         *
         * @param sheet the image that will represent us.
         */
        private setDimensions = (sheet : SpriteSheet) : void =>
        {
            // Set our dimensions and set the origin to be the center of the paddle.
            this._width = sheet.width;
            this._height = sheet.height;
            this._origin.setToXY (this._width / 2, this._height / 2);
        };

        /**
         * Set the ball that this paddle should track, or (when ball is null) stop tracking and return
         * player control.
         *
         * When the paddle is tracking a ball, every frame update it will attempt to shift its direction
         * in order to intercept the ball at the location it expects it to cross the vertical axis of this
         * paddle.
         *
         * Invoking this method causes the paddle to immediately calculate and start moving towards its
         * desired location; once the paddle gets there it will stop moving. Invoke updateTracking() every
         * time the ball changes directions in order to allow the paddle to update itself accordingly.
         *
         * @param ball the ball that we should track
         * @see updateTracking
         */
        track (ball : Ball) : void
        {
            this._ball = ball;
            this.updateTracking ();
        }

        /**
         * Every time this is invoked, the paddle will automatically recalculate where it should be moving
         * to based on the current ball location and direction.
         *
         * This is implicitly invoked every time the paddle is told to track a ball. However, the paddle
         * only updates its position based on the current direction of the ball (left or right); every
         * time the ball changes its direction, you should invoke this to tell the paddle so that it knows
         * what to do next.
         *
         * @see track
         */
        updateTracking () : void
        {
            // If we are tracking a ball, calculate a new position.
            if (this._ball != null)
                this.updateTargetPosition ();
        }

        /**
         * This analyzes the current ball location in relation to the paddle to determine where exactly we
         * should attempt to move the paddle to so that we will return the shot.
         *
         * This calculates the trajectory of the ball where it will cross our location by determining
         * where any interim bounces will be.
         */
        private updateTargetPosition () : void
        {
            // If the ball is moving away from us, we set our target position to the center of the stage
            // so that we are better positioned to return the ball.
            //
            // In order to determine this we check the sign of the X velocity of the ball and (crudely)
            // check what half of the screen we're on to know if we care about left or right.
            //
            // This last bit is important because based on how our collision detection works, the ball
            // might be slightly behind our actual X position, in which case we will miss that it's going
            // away from us because it appears to be coming towards us.

            // If the ball is moving away from us, our target position should be the center of the stage,
            // so that we are positioned as best as possible to hit the next shot.
            if ((this._position.x <= this._stage.width / 2 && this._ball.velX > 0) ||
                (this._position.x > this._stage.width / 2 && this._ball.velX < 0))
            {
                this._targetY = this._stage.height / 2;
            }
            else
            {
                // The ball must be moving towards us. In that case, we should attempt to determine the
                // location where the ball will eventually cross our side of the screen, so that we can move
                // there to return the ball.  This takes into account the ball will take even if the return
                // is going to bounce one or more times before it eventually gets to us.  Determine the X
                // coordinate of the computer paddle. This is always the same value, but putting it in a
                // variable makes the code less ugly below.
                let compPaddlePos = this._position.x;

                // The trajectory of the ball is just a regular line. The equation for a line is:
                //     y = mx + b
                //
                // Where m is the slope of the line (rise over run or change in y over change in x) and b is
                // the point at which the line intercepts the Y axis.  In order to determine where the ball
                // is going to intersect, we first need to determine the slope of the line.
                var slope = this._ball.velY / this._ball.velX;

                // The trajectory of the ball is a line and the point that the ball is on is on it. However,
                // since the line might reflect off of the top and bottom of the screen, the positions of the
                // ball also change. So we have temporary copies here. For the calculations below, we need an
                // X point to use. We will start with the location of the ball.
                var tmpBallX = this._ball.position.x;
                var tmpBallY = this._ball.position.y;

                // We need to know the y axis intercept of the ball trajectory; however, this will change
                // depending on the direction that the ball is going, and the ball will change directions if
                // it reflects off of the top or bottom of the screen.  So, we keep looping trying to
                // calculate the eventual position of the ball until it is in range.
                do
                {
                    // Calculate b, the Y axis intercept. If we rearrange the equation of the line a little
                    // bit, we end up with:  b = y - mx  We know the slope and the location of the ball, so
                    // we can calculate the intercept. Note that the intercept changes when the slope changes
                    // from negative to positive, which is why this is inside the loop.
                    var yIntercept = tmpBallY - (slope * tmpBallX);

                    // Now calculate what the Y position will be when the ball reaches the paddle location.
                    this._targetY = (slope * compPaddlePos) + yIntercept;

                    // The target value will be out of range of the canvas height if the trajectory of the
                    // ball is steep enough to bounce off of the top or the bottom. If that happens, then we
                    // need to loop again.
                    if (this._targetY < 0 || this._targetY >= this._stage.height)
                    {
                        // First, set the new ball location to be either the top or bottom of the screen.
                        if (this._targetY < 0)
                            tmpBallY = 0;
                        else
                            tmpBallY = this._stage.height - 1;

                        // Now, using the slope and intercept that we have, insert that Y value to determine
                        // where the X value of the bounce was.  The equation is reworked as:  x = (y - b) /
                        // x
                        tmpBallX = (tmpBallY - yIntercept) / slope;

                        // Reflect the slope of the trajectory to account for the bounce.
                        slope *= -1;
                    }
                }
                while (this._targetY < 0 || this._targetY >= this._stage.height);
            }

            // At this point, we know exactly where the ball will cross the paddle line. However, going
            // straight there is bad for a few reasons:
            //   1) The computer is pretty much unbeatable because it plays too perfect.
            //   2) The Y trajectory of the ball reflection with the paddle is governed by where on the paddle
            //      the ball hits. The center means a straight line out, which is boring.
            //
            // For these reasons (at least), we perturb the actual computed Y location to target by adding a
            // random factor to the final location, either up or down (clipping as needed). This will make the
            // computer sometimes miss the ball and when it does hit, it will bounce more unpredictably.
            //
            // This also has the effect of masking how perfectly the paddle zeroes in on the center of the
            // field after it returns the ball.
            this._targetY += Utils.randomIntInRange (-(this._height * PADDLE_MISS_FUDGE),
                                                     (this._height * PADDLE_MISS_FUDGE));
            if (this._targetY < 0)
                this._targetY = 0;
            else if (this._targetY >= this._stage.height)
                this._targetY = this._stage.height - 1;
        }

        /**
         * This is invoked every frame to update this paddle.
         *
         * If we are currently tracking a ball, then this method will use the current location and
         * velocity of the ball to attempt to intercept the ball. Otherwise this does nothing.
         *
         * @param stage the stage the paddle is on
         * @param tick the game tick, for timing purposes
         */
        update (stage : Stage, tick : number) : void
        {
            // If we are not tracking a ball, do nothing; a human is controlling this paddle.
            if (this._ball == null)
                return;

            // Try to move in the direction of our calculated intercept location. We will only actually
            // move if we're not in any danger of going past our target. This adds a little extra chance
            // to how the ball will react when (if) it hits us.
            if (this._position.y < this._targetY - PADDLE_COMPUTER_SPEED)
                this.move (PADDLE_COMPUTER_SPEED);
            else if (this._position.y > this._targetY + PADDLE_COMPUTER_SPEED)
                this.move (-PADDLE_COMPUTER_SPEED);
        }

        /**
         * Jump this paddle directly so that its vertical position is the position passed in. This does
         * limit checking to ensure the position is set to be as valid as possible.
         *
         * @param newPos the new Y position.
         */
        jumpTo (newPos : number) : void
        {
            // Set the current Y position and make sure that it is clamped to an acceptable range.
            this._position.y = Utils.clampToRange (newPos,
                                                   PADDLE_EDGE_MOVE_LIMIT,
                                                   this._stage.height - PADDLE_EDGE_MOVE_LIMIT);

        }

        /**
         * Shift this paddle up or down on the screen by the provided amount.
         *
         * Bounds checks are done to ensure that the paddle stops half way off the top or bottom of the stage.
         *
         * @param amount the amount to move (positive for down, negative for up).
         */
        move (amount : number) : void
        {
            // Adjust the current position by the provided amount and make sure that it is clamped to an
            // acceptable range.
            this.jumpTo (this._position.y + amount);
        }
    }
}
