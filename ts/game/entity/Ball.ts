module nurdz.game
{
    /**
     * Objects that follow this interface are used to get the results of a collision with other collider
     * objects on the screen. This tells us the position that the collision happened at on the bounds of
     * the collision object, and if the collision was horizontal or vertical.
     */
    export interface BallCollision
    {
        /**
         * The object that we collided with.
         */
        other : Collider;

        /**
         * The position on the bounds of the other object that the collision occurred at; this can be a
         * point along any of the 4 edges of the collider object; use the horizontal property to determine
         * if it's the X or Y position you care about, if needed.
         */
        position : Point;

        /**
         * Indicates the type of reflection that results from this collision, horizontal or vertical. A
         * horizontal reflection means that the ball collided with the left or right side of the other
         * object, while a vertical reflection means that the ball collided with the top or bottom of the
         * other object.
         */
        horizontal : boolean;
    }

    /**
     * A range of possible vertical speed values for the tennis ball to move when a new serve is made; either
     * after a score or at the start of the game.
     *
     * The speed will be selected between the first and last array elements, inclusive.
     * @const
     * @type {number[]}
     */
    const BALL_STARTSPEED_Y = [-18, 18];

    /**
     * A range of possible horizontal speed values for the tennis ball to move when a new serve is made;
     * either after a score or at the start of the game.
     *
     * The speed will be selected between the first and last array elements, inclusive.
     *
     * NOTE: The serve is made by the player that was just scored on, and as a result the speed values
     * specified should be positive, with the direction being chosen based on the most recent scoring
     * situation.
     *
     * @const
     * @type {number[]}
     */
    const BALL_STARTSPEED_X = [10, 20];

    /**
     * Specifies how many times the ball has to bounce off of a paddle (either will do) before the horizontal
     * speed will get kicked up to a higher value.
     *
     * This tunes the difficulty of the game by modifying the speed of the ball as the volley continues.
     *
     * @const
     * @type {number}
     */
    const BALL_BOUNCE_TIMES = 4;

    /**
     * Specifies the modification to the horizontal speed of the ball every time the speed is kicked up by an
     * ongoing volley.
     *
     * @const
     * @see BALL_BOUNCE_TIMES
     * @type {number}
     */
    const BALL_BOUNCE_ACCELERATION = 1.25;

    /**
     * The size of the trail that follows the ball. This controls how many previous moves of the ball are
     * displayed on the screen to indicate it's motion path.
     *
     * @type {number}
     */
    const BALL_TRAIL_MAXLEN = 5;

    /**
     * This entity represents the ball in the game.
     */
    export class Ball extends Entity
    {
        /**
         * The parent scene that owns us and should be notified when things happen.
         */
        private _parent : GameScene;

        /**
         * The velocity of the ball in the X and Y dimensions
         */
        private _speed : Vector2D;

        /**
         * The number of times the ball has bounces off of a paddle during the current volley (this gets
         * reset when the ball gets reset at the start of a serve).
         *
         * This is used to track when the horizontal speed should be kicked up as the volley continues.
         */
        private _bounces : number;

        /**
         * The list of entities that the ball can collide with. Every time we move, we check to
         * see if we are colliding with one of these entities and act accordingly. Simplistically (since
         * this is pong) the only reaction to a collision with a collider is to reflect the X portion of
         * our movement vector.
         */
        private _colliders : Array<Entity>;

        /**
         * This is an array of previous positions; every time the ball resets, it is emptied, but it
         * fills up to a size of BALL_TRAIL_MAXLEN with points that represent the previous positions of
         * the ball.
         *
         * This is treated as a circular list, so once the array is filled up, the existing points are
         * modified instead of being created as new ones.
         */
        private _trail : Array<Point>;

        /**
         * This indicates where the most recent position update has been stored in the trail circular list.
         * It starts at -1 when the ball first starts moving after a reset and then increments, wrapping
         * around at the end of the array.
         */
        private _trailHeadPos : number;

        /**
         * This indicates the oldest position of the ball as stored in our circular list. It starts at 0
         * and increments on every ball move after headPos has moved through the list enough to have a
         * complete movement history.
         */
        private _trailTailPos : number;

        /**
         * Get the current X velocity of the ball
         *
         * @returns {number}
         */
        get velX () : number
        { return this._speed.x; }

        /**
         * Get the current Y velocity of the ball
         *
         * @returns {number}
         */
        get velY () : number
        { return this._speed.y; }

        /**
         * Construct a new ball that will render on the stage provided.
         *
         * @param stage the stage the ball will be on
         * @param parent the scene that owns us and will be notified of events as they occur
         */
        constructor (stage : Stage, parent : GameScene)
        {
            // Invoke the super. Note that we don't provide any location or dimensions here. These will
            // get set later.
            super ("ball", stage, 0, 0, 0, 0, 1, {}, {}, 'red');

            // Save our parent
            this._parent = parent;

            // Load the sprite sheet that contains the ball images. The size of our entity is determined
            // by the size of the sprites, so we let the callback handle that.
            this._sheet = new SpriteSheet (stage, "ball_3_1.png", 3, 1, true, this.setDimensions);

            // Create the vector that will hold our speed.
            this._speed = new Vector2D (0, 0);

            // Initialize our collider list.
            this._colliders = [];

            // Init everything else.
            this.reset ();
        }

        /**
         * This gets invoked when our ball sprite sheet finishes preloading. We use this in order to determine
         * what our width and height are.
         *
         * @param sheet the sprite sheet that will represent us
         */
        private setDimensions = (sheet : SpriteSheet) : void =>
        {
            // Convert our collision volume to be a circle with the origin at its center.
            this.makeCircle (sheet.width / 2, true);
        };

        /**
         * Register a new collider with the ball; a collider is something that can reflect the horizontal
         * movement of the ball.
         *
         * @param collider the collider to add
         */
        addCollider (collider : Entity) : void
        {
            this._colliders.push (collider);
        }

        /**
         * Reset the ball properties for a serve, which includes the position, speed and so on.
         */
        public reset () : void
        {
            // Reset the ball trail information.
            this._trail = [];
            this._trailHeadPos = -1;
            this._trailTailPos = 0;

            // Set up a random speed for the ball. We flip the sign on the X speed of the ball relative to
            // what it is currently so that the serve alternates sides when the ball resets.
            this._speed.x = Utils.randomIntInRange (BALL_STARTSPEED_X[0],
                                                    BALL_STARTSPEED_X[1]) * (this._speed.x < 0 ? -1 : 1);
            this._speed.y = Utils.randomIntInRange (BALL_STARTSPEED_Y[0], BALL_STARTSPEED_Y[1]);

            // Revert back to the initial ball image and reset the bounce count.
            this._sprite = 0;
            this._bounces = 0;

            // Flip the ball direction and then center it on the stage. We center vertically, but
            // horizontally by fourths, with the bias being to the side the ball is serving from.
            this._speed.flipX ();
            this.setStagePositionXY ((this._speed.x > 0)
                                         ? (this._stage.width / 4)
                                         : (this._stage.width - (this._stage.width / 4)),
                                     this._stage.height / 2);
        }

        /**
         * This method assumes that the update() method has just finished shifting our location and checks
         * to see if we have interested with one of the collision objects or not.
         *
         * This checks everywhere along the vector described by our current speed vector, so that if we're
         * going too fast and skip through the paddle, we can still detect the collision.
         *
         * If there is no collision, then null is returned. Otherwise, our position is updated to be just
         * before the collision occurred, and an object is returned that indicates the position at which
         * the collision occurred and whether it was a horizontal or vertical edge.
         *
         * @returns {BallCollision} null if there is no collision, or an object that indicates the exact
         * collision location and a boolean that tells you if the reflection should be horizontal or
         * vertical as a result of the collision.
         */
        private checkColliders () : BallCollision
        {
            // Iterate over all of our colliders. The first one we collide with, return its Y position.
            for (let i = 0 ; i < this._colliders.length ; i++)
            {
                // Alias the collider to make the code below more readable (and maybe faster because no
                // array access? I dunno).
                let other = this._colliders[i];

                // We don't want to collide with an object if we're inside of it, because then the collision
                // point would be on the wrong edge. However it's possible for the ball to be inside a
                // collider due to the order in which things move.
                //
                // Since this is a simple game, the easiest solution is to only allow collisions with
                // objects that we are moving towards; that allows a defender to move the paddle onto the
                // ball at the last possible frame (where the ball would be inside the paddle) without
                // having problems like having us register a collision as we try to move out.
                //
                // Here we check to see if the X position of this object relative to the center of the
                // stage has the same sign as our speed vector's X component. If they do, then we are
                // moving towards it and we can collide. Otherwise we skip it.
                if (Math.sign (this._speed.x) != Math.sign (other.position.x - (this._stage.width / 2)))
                    continue;

                // We get called after the ball has already moved. Our first check is, "was the position
                // that the ball was in at the start of the frame inside the paddle?"
                //
                // If it is, then any intersection we're about to get is going to be as the ball exits the
                // paddle, and we want to ignore that. This can happen if the paddle moves after the ball
                // moves in the last frame and puts the ball inside of itself. This either means that the
                // paddle moved to block the ball too late OR that the collision was on the top/bottom of
                // the paddle, in which case the paddle moved onto it after it turned around.
                if (other.containsXY (this._position.x - this._speed.x, this._position.y - this._speed.y))
                    continue;

                // The ball wasn't inside the paddle when it started, so see if it ends up inside the paddle
                // or passes through it by checking the line segment representing its motion against this
                // collision object.
                //
                // The directionality of the speed vector inherently allows the collision code to determine
                // where the collision happened.
                let intersect = other.intersectWithSegmentXY (this._position.x - this._speed.x,
                                                              this._position.y - this._speed.y,
                                                              this._position.x, this._position.y);

                // If we did intersect, then we need to do some work.
                if (intersect != null)
                {
                    // Set the position of the ball to be exactly where the collision happened along the
                    // paddle. Since our origin is right in the middle, this will place the ball half
                    // inside the paddle, but since it goes so fast this is OK.
                    this._position.setTo (intersect);

                    // Get the origin-corrected bounding position of the collision object that we hit.
                    let left = other.position.x - other.origin.x;
                    let top = other.position.y - other.origin.y;
                    let right = left + other.width;
                    let bottom = top + other.height;

                    // Now return back an object that says what we collided with, where, and what kind of
                    // reflection is needed.
                    return <BallCollision> {
                        other:      other,
                        position:   intersect,
                        horizontal: intersect.x == left || intersect.x == right
                    };
                }
            }

            // Not colliding with anything.
            return null;
        }

        /**
         * Store the current position of the ball into our circular ball trail array, updating the head
         * and tail positions as appropriate.
         */
        private storeTrailPosition () : void
        {
            // The head position stores the current location of the ball and the tail stores the farthest
            // back position; we always update the head but we only update the tail when we have a
            // complete movement history stored in our buffer.
            //
            // Since the buffer is circular, we make sure our positions wrap around.
            this._trailHeadPos = (this._trailHeadPos + 1) % BALL_TRAIL_MAXLEN;
            if (this._trail.length == BALL_TRAIL_MAXLEN)
                this._trailTailPos = (this._trailTailPos + 1) % BALL_TRAIL_MAXLEN;

            // Store the current position; if this element does not exist, make a copy of the current
            // position, otherwise just set the location. We don't want to create an excessive amount of
            // point objects.
            if (this._trail[this._trailHeadPos] == null)
                this._trail[this._trailHeadPos] = this._position.copy ();
            else
                this._trail[this._trailHeadPos].setTo (this._position);
        }

        /**
         * This is called every frame update (tick tells us how many times this has happened) to allow us
         * to update our position.
         *
         * @param stage the stage that we are on
         * @param tick the current engine tick; this advances one for each frame update
         */
        update (stage : Stage, tick : number) : void
        {
            // Translate the ball position by our speed.
            this._position.translateXY (this._speed.x, this._speed.y);

            // Check if we should bounce off of the ceiling or floor of the stage. This is adjusted by the
            // radius of ourselves.
            if ((this._position.y >= stage.height - this.radius && this._speed.y > 0) ||
                (this._position.y <= this.radius && this._speed.y < 0))
            {
                // Make a reversed copy of our speed vector and then set its magnitude to be how far back
                // along it we need to go before we have moved enough on the Y axis to get our position to
                // be exactly our radius away from the edge of the stage.
                //
                // This will make us visually stop right at the edge of the screen.
                let revSpeed = this._speed.copyReversed ();
                let axialDisplacement = (this._position.y <= this.radius
                    ? this.radius - this._position.y
                    : this.position.y - (stage.height - this.radius));
                revSpeed.magnitude = Math.abs (axialDisplacement / Math.sin (revSpeed.direction));

                // Translate backwards along this new vector to put our edge right at the edge of the screen.
                this._position.translate (revSpeed);

                // Now we can reflect our Y speed to change directions and let our parent know that we
                // bounced off a wall.
                this._speed.flipY ();
                this._parent.wallBounce ();
            }

            // Check to see if we're colliding with any of our collision objects or not.
            //
            // When a collision is found, we get told the exact location it happened and whether or not it
            // was with a horizontal or vertical edge. Additionally our position is pushed backwards along
            // our current speed vector to put us just outside of the bounds of what we hit.
            //
            // null is returned if we didn't hit anything.
            let collideInfo = this.checkColliders ();
            if (collideInfo != null)
            {
                // Does this require a horizontal reflection?
                if (collideInfo.horizontal == true)
                {
                    // Yes, so flip our horizontal speed to go in the other direction.
                    this._speed.flipX ();

                    // Now we alter our vertical speed based on where along the collision object we made
                    // impact. This is 0 plus or minus half the height of the collided object, linearly
                    // varying. We use a damping value so that the ball doesn't go too nuts.
                    this._speed.y = (this.position.y - collideInfo.other.position.y) * PADDLE_EDGESPEED_DAMPER;

                    // Since we're reflecting back, get the parent to tell the paddles to calculate where
                    // to go next. This doesn't happen for a vertical strike because there's no way to
                    // return it.
                    this._parent.reflect ();
                }
                else
                {
                    // No, this is a collision coming off of the top or bottom of the paddle. In this
                    // case, just reflect our vertical direction instead, with no speed change.
                    //
                    // In this case the serve is not returnable because it's still moving left or right,
                    // but now it's going away from the paddle; so, don't tell the paddles to try and
                    // calculate a position here since it's pointless.
                    this._speed.flipY ();
                }

                // This counts as a bounce of the ball; If the ball has bounced enough times, kick up the
                // speed of the ball and change its color.
                this._bounces++;
                if (this._bounces % BALL_BOUNCE_TIMES == 0)
                {
                    // Speed up the ball.
                    this._speed.x *= BALL_BOUNCE_ACCELERATION;

                    // If the ball is not already at our last image, bump it up to the next one.
                    if (this._sprite < this._sheet.count - 1)
                        this._sprite++;
                }

                // Now tell our parent that the ball has reflected. This has to happen AFTER the reflection
                // happens because things that care about reflections nominally want to know the new location
                // of the ball without having to calculate that themselves.
                // this._parent.reflect ();
            }

            // Check if we are hitting the left or right side of the stage (or going off of it).
            //
            // When this occurs, someone is scoring points.
            if ((this._position.x >= stage.width && this._speed.x > 0) ||
                (this._position.x <= 0 && this._speed.x < 0))
            {
                // Tell our parent that the ball is moving into a scoring position.
                this._parent.score ();
            }

            // Store the current ball position in the ball trail array. This needs to happen here and not
            // right after we move the ball, because intersections and bounces may cause the position to
            // be updated a bit.
            this.storeTrailPosition ();
        }

        /**
         * Render the ball at the provided location and using the given renderer.
         *
         * We assume our position is at our center.
         *
         * @param x the x of the center of the ball
         * @param y the y of the center of the ball
         * @param renderer the renderer to blit with
         */
        render (x : number, y : number, renderer : Renderer) : void
        {
            // If there is any stored trail information, render that first, back to front to get the
            // appropriate overlap.
            if (this._trail.length > 0)
            {
                for (let arrPos = this._trailTailPos ; arrPos != this._trailHeadPos ; arrPos = (arrPos + 1) % BALL_TRAIL_MAXLEN)
                {
                    var pos = this._trail[arrPos];
                    this._sheet.blitCentered (this._sprite, pos.x, pos.y, renderer);
                }
            }

            // Now render the ball at the location provided, which is our current location. We let the
            // super do that for us.
            super.render (x, y, renderer);
        }
    }
}
