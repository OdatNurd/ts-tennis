module nurdz.game
{
    /**
     * The number of points that need to be scored by one player in order to win the game when a regular
     * game is in progress.
     *
     * @type {number}
     */
    export const WINNING_SCORE = 11;

    /**
     * The number of points that need to be scored by one player in order to "win" the game and go back to
     * the title screen in attract mode.
     *
     * @type {number}
     */
    export const ATTRACT_SCORE = 3;

    /**
     * The number of people playing the game; this can be 0 through 2; 0 means the computer is playing
     * itself "attract mode").
     *
     * @type {number}
     */
    export var playerCount : number = 0;

    /**
     * The number of points player 1 (the left player) has scored for this game.
     *
     * @type {number}
     */
    export var player1Score : number = 0;

    /**
     * The number of points player 2 (the right player) has scored for this game.
     *
     * @type {number}
     */
    export var player2Score : number = 0;

    /**
     * Fetch the size of the largest possible score, so that we can position our scores as appropriate
     * when we render them.
     *
     * @type {Point}
     */
    let fontDimensions = numberStringSize (WINNING_SCORE + "");

    /**
     * The X position to draw the left score at. Calculated via setPaddleWidths().
     *
     * This default value is "good enough" as a default in case setPaddleWidths() is not called (say
     * during debugging).
     *
     * @type {number}
     * @see setPaddleWidths
     */
    let leftScoreX : number = 100;

    /**
     * The X position to draw the right score at. Calculated via setPaddleWidths().
     *
     * This default value is "good enough" as a default in case setPaddleWidths() is not called (say
     * during debugging).
     *
     * @type {number}
     * @see setPaddleWidths
     */
    let rightScoreX : number = 700;

    /**
     * Set the width of the left and right paddles; these are values used when rendering the score values
     * so that we know how to position them.
     *
     * This needs to be invoked at least once prior to scores being displayed.
     *
     * @param stage the stage that the paddles will render on
     * @param leftWidth the width of the left paddle
     * @param rightWidth the width of the right paddle
     */
    export function setPaddleWidths (stage : Stage, leftWidth : number, rightWidth : number) : void
    {
        // Calculate offsets that  offset the score out of the way of the location that the paddles are
        // displayed at. This is more complicated for the right paddle because we need to take into
        // account the width of the resulting rendered scores to know where the position should be.
        leftScoreX = PADDLE_EDGESPACE + (2 * leftWidth);
        rightScoreX = stage.width - PADDLE_EDGESPACE - (2 * rightWidth) - fontDimensions.x;
    }

    /**
     * Reset the scores of both players back to 0.
     */
    export function resetScores () : void
    {
        player1Score = player2Score = 0;
    }

    /**
     * Render the current game scores to the stage using the renderer provided.
     *
     * This always renders the scores as currently set, in a known size, at a known location.
     *
     * @param renderer the renderer to use to render the data.
     */
    export function renderScores (renderer : CanvasRenderer) : void
    {
        renderNumber (renderer, leftScoreX, 20, 'white', player1Score + "");
        renderNumber (renderer, rightScoreX, 20, 'white', player2Score + "");
    }
}
