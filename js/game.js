var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * An object which maps digits into polygons that can be rendered for a simple numeric display.
         * The polygon data assumes that the top left of all of the character cells is 0,0 and that each level
         * is FONT_WIDTH x FONT_HEIGHT units in dimension.
         *
         * As such, you probably want to draw this scaled; note that when you scale the canvas, the location
         * of things rendered is scaled as well. For the purposes of our font, this works out OK.
         *
         * @type {Object<string,Polygon>}
         */
        var NUMBER_FONT = {
            "0": [['m', 0, 0], [3, 0], [3, 5], [0, 5], ['c'], ['m', 1, 1], [1, 4], [2, 4], [2, 1]],
            "1": [['m', 1, 0], [2, 0], [2, 5], [1, 5]],
            "2": [['m', 0, 0], [3, 0], [3, 3], [1, 3], [1, 4], [3, 4], [3, 5], [0, 5], [0, 2], [2, 2], [2, 1],
                [0, 1]],
            "3": [['m', 0, 0], [3, 0], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3], [0, 3], [0, 2], [2, 2], [2, 1],
                [0, 1]],
            "4": [['m', 0, 0], [1, 0], [1, 2], [2, 2], [2, 0], [3, 0], [3, 5], [2, 5], [2, 3], [0, 3]],
            "5": [['m', 0, 0], [3, 0], [3, 1], [1, 1], [1, 2], [3, 2], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3],
                [0, 3]],
            "6": [['m', 0, 0], [3, 0], [3, 1], [1, 1], [1, 2], [3, 2], [3, 5], [0, 5], ['c'], ['m', 1, 3], [1, 4],
                [2, 4], [2, 3]],
            "7": [['m', 0, 0], [3, 0], [3, 5], [2, 5], [2, 1], [1, 1], [1, 2], [0, 2]],
            "8": [['m', 0, 0], [3, 0], [3, 5], [0, 5], ['c'], ['m', 1, 1], [1, 2], [2, 2], [2, 1], ['c'],
                ['m', 1, 3],
                [1, 4], [2, 4], [2, 3]],
            "9": [['m', 0, 0], [3, 0], [3, 5], [0, 5], [0, 4], [2, 4], [2, 3], [0, 3], ['m', 1, 1], [1, 2],
                [2, 2],
                [2, 1]]
        };
        /**
         * How many "units" wide the NUMBER_FONT font data points think that it is.
         *
         * @type {number}
         */
        var FONT_WIDTH = 3;
        /**
         * How many "units" tall the NUMBER_FONT font data points think that it is.
         * @type {number}
         */
        var FONT_HEIGHT = 5;
        /**
         * How many "units" should appear between consecutive NUMBER_FONT digits when they are rendered.
         *
         * @type {number}
         */
        var FONT_SPACING = 0.5;
        /**
         * This sets how big each unit in the font is when it is rendered. Thus each character in the font
         * will be FONT_WIDTH * FONT_SCALE pixels wide and FONT_HEIGHT * FONT_SCALE pixels tall. Set as
         * appropriate.
         *
         * Here we are saying that the overall height of a character is 64 pixels, so we need to divide that
         * by the height of the font in "font units".
         *
         * @type {number}
         */
        var FONT_SCALE = 64 / FONT_HEIGHT;
        /**
         * Crudely render a number using our number font.
         *
         * @param renderer the renderer to use to render the number
         * @param x the x position to render the top left of the number at
         * @param y the y position to render the top left of the number at
         * @param color the color to render the number
         * @param numString the string to render, which needs to be only digits.
         */
        function renderNumber(renderer, x, y, color, numString) {
            for (var i = 0; i < numString.length; i++, x += (FONT_WIDTH * FONT_SCALE) + (FONT_SPACING * FONT_SCALE)) {
                // Translate to where the number should start rendering, then scale the canvas. Since the font
                // data assumes 1 pixels per unit, the scale sets how many pixels wide each unit turns out.
                renderer.translateAndRotate(x, y);
                renderer.context.scale(FONT_SCALE, FONT_SCALE);
                var polygon = NUMBER_FONT[numString[i]];
                if (polygon)
                    renderer.fillPolygon(polygon, color);
                renderer.restore();
            }
        }
        game.renderNumber = renderNumber;
        /**
         * Given a string of digits, return back a point where the X value indicates how many pixels wide
         * and tall the rendered polygon for that text would be.
         *
         * @param numberStr the string to calculate the size of
         * @returns {Point} a point whose x value is the width of the string in pixels and whose y is the
         * height in pixels.
         */
        function numberStringSize(numberStr) {
            // Get the height and width of a digit in our number font in pixels based on the scale factor.
            var pixelWidth = FONT_WIDTH * FONT_SCALE;
            var pixelHeight = FONT_HEIGHT * FONT_SCALE;
            var pixelGap = FONT_SPACING * FONT_SCALE;
            return new game.Point(numberStr.length * pixelWidth + (numberStr.length - 1) * pixelGap, pixelHeight);
        }
        game.numberStringSize = numberStringSize;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The number of points that need to be scored by one player in order to win the game when a regular
         * game is in progress.
         *
         * @type {number}
         */
        game.WINNING_SCORE = 11;
        /**
         * The number of points that need to be scored by one player in order to "win" the game and go back to
         * the title screen in attract mode.
         *
         * @type {number}
         */
        game.ATTRACT_SCORE = 3;
        /**
         * The number of people playing the game; this can be 0 through 2; 0 means the computer is playing
         * itself "attract mode").
         *
         * @type {number}
         */
        game.playerCount = 0;
        /**
         * The number of points player 1 (the left player) has scored for this game.
         *
         * @type {number}
         */
        game.player1Score = 0;
        /**
         * The number of points player 2 (the right player) has scored for this game.
         *
         * @type {number}
         */
        game.player2Score = 0;
        /**
         * Fetch the size of the largest possible score, so that we can position our scores as appropriate
         * when we render them.
         *
         * @type {Point}
         */
        var fontDimensions = game.numberStringSize(game.WINNING_SCORE + "");
        /**
         * The X position to draw the left score at. Calculated via setPaddleWidths().
         *
         * This default value is "good enough" as a default in case setPaddleWidths() is not called (say
         * during debugging).
         *
         * @type {number}
         * @see setPaddleWidths
         */
        var leftScoreX = 100;
        /**
         * The X position to draw the right score at. Calculated via setPaddleWidths().
         *
         * This default value is "good enough" as a default in case setPaddleWidths() is not called (say
         * during debugging).
         *
         * @type {number}
         * @see setPaddleWidths
         */
        var rightScoreX = 700;
        /**
         * Set the width of the left and right paddles; these are values used when rendering the score values
         * so that we know how to position them.
         *
         * This needs to be invoked at least once prior to scores being displayed.
         *
         * @param stage the stage that the paddles will render on
         * @param leftWidth the width of the left paddle
         * @param rightWidth the width of the right paddle
         */
        function setPaddleWidths(stage, leftWidth, rightWidth) {
            // Calculate offsets that  offset the score out of the way of the location that the paddles are
            // displayed at. This is more complicated for the right paddle because we need to take into
            // account the width of the resulting rendered scores to know where the position should be.
            leftScoreX = game.PADDLE_EDGESPACE + (2 * leftWidth);
            rightScoreX = stage.width - game.PADDLE_EDGESPACE - (2 * rightWidth) - fontDimensions.x;
        }
        game.setPaddleWidths = setPaddleWidths;
        /**
         * Reset the scores of both players back to 0.
         */
        function resetScores() {
            game.player1Score = game.player2Score = 0;
        }
        game.resetScores = resetScores;
        /**
         * Render the current game scores to the stage using the renderer provided.
         *
         * This always renders the scores as currently set, in a known size, at a known location.
         *
         * @param renderer the renderer to use to render the data.
         */
        function renderScores(renderer) {
            game.renderNumber(renderer, leftScoreX, 20, 'white', game.player1Score + "");
            game.renderNumber(renderer, rightScoreX, 20, 'white', game.player2Score + "");
        }
        game.renderScores = renderScores;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * A range of possible vertical speed values for the tennis ball to move when a new serve is made; either
         * after a score or at the start of the game.
         *
         * The speed will be selected between the first and last array elements, inclusive.
         * @const
         * @type {number[]}
         */
        var BALL_STARTSPEED_Y = [-18, 18];
        /**
         * A range of possible horizontal speed values for the tennis ball to move when a new serve is made;
         * either after a score or at the start of the game.
         *
         * The speed will be selected between the first and last array elements, inclusive.
         *
         * NOTE: The serve is made by the player that was just scored on, and as a result the speed values
         * specified should be positive, with the direction being chosen based on the most recent scoring
         * situation.
         *
         * @const
         * @type {number[]}
         */
        var BALL_STARTSPEED_X = [10, 20];
        /**
         * Specifies how many times the ball has to bounce off of a paddle (either will do) before the horizontal
         * speed will get kicked up to a higher value.
         *
         * This tunes the difficulty of the game by modifying the speed of the ball as the volley continues.
         *
         * @const
         * @type {number}
         */
        var BALL_BOUNCE_TIMES = 4;
        /**
         * Specifies the modification to the horizontal speed of the ball every time the speed is kicked up by an
         * ongoing volley.
         *
         * @const
         * @see BALL_BOUNCE_TIMES
         * @type {number}
         */
        var BALL_BOUNCE_ACCELERATION = 1.25;
        /**
         * The size of the trail that follows the ball. This controls how many previous moves of the ball are
         * displayed on the screen to indicate it's motion path.
         *
         * @type {number}
         */
        var BALL_TRAIL_MAXLEN = 5;
        /**
         * This entity represents the ball in the game.
         */
        var Ball = (function (_super) {
            __extends(Ball, _super);
            /**
             * Construct a new ball that will render on the stage provided.
             *
             * @param stage the stage the ball will be on
             * @param parent the scene that owns us and will be notified of events as they occur
             */
            function Ball(stage, parent) {
                var _this = this;
                // Invoke the super. Note that we don't provide any location or dimensions here. These will
                // get set later.
                _super.call(this, "ball", stage, 0, 0, 0, 0, 1, {}, {}, 'red');
                /**
                 * This gets invoked when our ball sprite sheet finishes preloading. We use this in order to determine
                 * what our width and height are.
                 *
                 * @param sheet the sprite sheet that will represent us
                 */
                this.setDimensions = function (sheet) {
                    // Convert our collision volume to be a circle with the origin at its center.
                    _this.makeCircle(sheet.width / 2, true);
                };
                // Save our parent
                this._parent = parent;
                // Load the sprite sheet that contains the ball images. The size of our entity is determined
                // by the size of the sprites, so we let the callback handle that.
                this._sheet = new game.SpriteSheet(stage, "ball_3_1.png", 3, 1, true, this.setDimensions);
                // Create the vector that will hold our speed.
                this._speed = new game.Vector2D(0, 0);
                // Initialize our collider list.
                this._colliders = [];
                // Init everything else.
                this.reset();
            }
            Object.defineProperty(Ball.prototype, "velX", {
                /**
                 * Get the current X velocity of the ball
                 *
                 * @returns {number}
                 */
                get: function () { return this._speed.x; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Ball.prototype, "velY", {
                /**
                 * Get the current Y velocity of the ball
                 *
                 * @returns {number}
                 */
                get: function () { return this._speed.y; },
                enumerable: true,
                configurable: true
            });
            /**
             * Register a new collider with the ball; a collider is something that can reflect the horizontal
             * movement of the ball.
             *
             * @param collider the collider to add
             */
            Ball.prototype.addCollider = function (collider) {
                this._colliders.push(collider);
            };
            /**
             * Reset the ball properties for a serve, which includes the position, speed and so on.
             */
            Ball.prototype.reset = function () {
                // Reset the ball trail information.
                this._trail = [];
                this._trailHeadPos = -1;
                this._trailTailPos = 0;
                // Set up a random speed for the ball. We flip the sign on the X speed of the ball relative to
                // what it is currently so that the serve alternates sides when the ball resets.
                this._speed.x = game.Utils.randomIntInRange(BALL_STARTSPEED_X[0], BALL_STARTSPEED_X[1]) * (this._speed.x < 0 ? -1 : 1);
                this._speed.y = game.Utils.randomIntInRange(BALL_STARTSPEED_Y[0], BALL_STARTSPEED_Y[1]);
                // Revert back to the initial ball image and reset the bounce count.
                this._sprite = 0;
                this._bounces = 0;
                // Flip the ball direction and then center it on the stage. We center vertically, but
                // horizontally by fourths, with the bias being to the side the ball is serving from.
                this._speed.flipX();
                this.setStagePositionXY((this._speed.x > 0)
                    ? (this._stage.width / 4)
                    : (this._stage.width - (this._stage.width / 4)), this._stage.height / 2);
            };
            /**
             * This method assumes that the update() method has just finished shifting our location and checks
             * to see if we have interested with one of the collision objects or not.
             *
             * This checks everywhere along the vector described by our current speed vector, so that if we're
             * going too fast and skip through the paddle, we can still detect the collision.
             *
             * If there is no collision, then null is returned. Otherwise, our position is updated to be just
             * before the collision occurred, and an object is returned that indicates the position at which
             * the collision occurred and whether it was a horizontal or vertical edge.
             *
             * @returns {BallCollision} null if there is no collision, or an object that indicates the exact
             * collision location and a boolean that tells you if the reflection should be horizontal or
             * vertical as a result of the collision.
             */
            Ball.prototype.checkColliders = function () {
                // Iterate over all of our colliders. The first one we collide with, return its Y position.
                for (var i = 0; i < this._colliders.length; i++) {
                    // Alias the collider to make the code below more readable (and maybe faster because no
                    // array access? I dunno).
                    var other = this._colliders[i];
                    // We don't want to collide with an object if we're inside of it, because then the collision
                    // point would be on the wrong edge. However it's possible for the ball to be inside a
                    // collider due to the order in which things move.
                    //
                    // Since this is a simple game, the easiest solution is to only allow collisions with
                    // objects that we are moving towards; that allows a defender to move the paddle onto the
                    // ball at the last possible frame (where the ball would be inside the paddle) without
                    // having problems like having us register a collision as we try to move out.
                    //
                    // Here we check to see if the X position of this object relative to the center of the
                    // stage has the same sign as our speed vector's X component. If they do, then we are
                    // moving towards it and we can collide. Otherwise we skip it.
                    if (Math.sign(this._speed.x) != Math.sign(other.position.x - (this._stage.width / 2)))
                        continue;
                    // We get called after the ball has already moved. Our first check is, "was the position
                    // that the ball was in at the start of the frame inside the paddle?"
                    //
                    // If it is, then any intersection we're about to get is going to be as the ball exits the
                    // paddle, and we want to ignore that. This can happen if the paddle moves after the ball
                    // moves in the last frame and puts the ball inside of itself. This either means that the
                    // paddle moved to block the ball too late OR that the collision was on the top/bottom of
                    // the paddle, in which case the paddle moved onto it after it turned around.
                    if (other.containsXY(this._position.x - this._speed.x, this._position.y - this._speed.y))
                        continue;
                    // The ball wasn't inside the paddle when it started, so see if it ends up inside the paddle
                    // or passes through it by checking the line segment representing its motion against this
                    // collision object.
                    //
                    // The directionality of the speed vector inherently allows the collision code to determine
                    // where the collision happened.
                    var intersect = other.intersectWithSegmentXY(this._position.x - this._speed.x, this._position.y - this._speed.y, this._position.x, this._position.y);
                    // If we did intersect, then we need to do some work.
                    if (intersect != null) {
                        // Set the position of the ball to be exactly where the collision happened along the
                        // paddle. Since our origin is right in the middle, this will place the ball half
                        // inside the paddle, but since it goes so fast this is OK.
                        this._position.setTo(intersect);
                        // Get the origin-corrected bounding position of the collision object that we hit.
                        var left = other.position.x - other.origin.x;
                        var top_1 = other.position.y - other.origin.y;
                        var right = left + other.width;
                        var bottom = top_1 + other.height;
                        // Now return back an object that says what we collided with, where, and what kind of
                        // reflection is needed.
                        return {
                            other: other,
                            position: intersect,
                            horizontal: intersect.x == left || intersect.x == right
                        };
                    }
                }
                // Not colliding with anything.
                return null;
            };
            /**
             * Store the current position of the ball into our circular ball trail array, updating the head
             * and tail positions as appropriate.
             */
            Ball.prototype.storeTrailPosition = function () {
                // The head position stores the current location of the ball and the tail stores the farthest
                // back position; we always update the head but we only update the tail when we have a
                // complete movement history stored in our buffer.
                //
                // Since the buffer is circular, we make sure our positions wrap around.
                this._trailHeadPos = (this._trailHeadPos + 1) % BALL_TRAIL_MAXLEN;
                if (this._trail.length == BALL_TRAIL_MAXLEN)
                    this._trailTailPos = (this._trailTailPos + 1) % BALL_TRAIL_MAXLEN;
                // Store the current position; if this element does not exist, make a copy of the current
                // position, otherwise just set the location. We don't want to create an excessive amount of
                // point objects.
                if (this._trail[this._trailHeadPos] == null)
                    this._trail[this._trailHeadPos] = this._position.copy();
                else
                    this._trail[this._trailHeadPos].setTo(this._position);
            };
            /**
             * This is called every frame update (tick tells us how many times this has happened) to allow us
             * to update our position.
             *
             * @param stage the stage that we are on
             * @param tick the current engine tick; this advances one for each frame update
             */
            Ball.prototype.update = function (stage, tick) {
                // Translate the ball position by our speed.
                this._position.translateXY(this._speed.x, this._speed.y);
                // Check if we should bounce off of the ceiling or floor of the stage. This is adjusted by the
                // radius of ourselves.
                if ((this._position.y >= stage.height - this.radius && this._speed.y > 0) ||
                    (this._position.y <= this.radius && this._speed.y < 0)) {
                    // Make a reversed copy of our speed vector and then set its magnitude to be how far back
                    // along it we need to go before we have moved enough on the Y axis to get our position to
                    // be exactly our radius away from the edge of the stage.
                    //
                    // This will make us visually stop right at the edge of the screen.
                    var revSpeed = this._speed.copyReversed();
                    var axialDisplacement = (this._position.y <= this.radius
                        ? this.radius - this._position.y
                        : this.position.y - (stage.height - this.radius));
                    revSpeed.magnitude = Math.abs(axialDisplacement / Math.sin(revSpeed.direction));
                    // Translate backwards along this new vector to put our edge right at the edge of the screen.
                    this._position.translate(revSpeed);
                    // Now we can reflect our Y speed to change directions and let our parent know that we
                    // bounced off a wall.
                    this._speed.flipY();
                    this._parent.wallBounce();
                }
                // Check to see if we're colliding with any of our collision objects or not.
                //
                // When a collision is found, we get told the exact location it happened and whether or not it
                // was with a horizontal or vertical edge. Additionally our position is pushed backwards along
                // our current speed vector to put us just outside of the bounds of what we hit.
                //
                // null is returned if we didn't hit anything.
                var collideInfo = this.checkColliders();
                if (collideInfo != null) {
                    // Does this require a horizontal reflection?
                    if (collideInfo.horizontal == true) {
                        // Yes, so flip our horizontal speed to go in the other direction.
                        this._speed.flipX();
                        // Now we alter our vertical speed based on where along the collision object we made
                        // impact. This is 0 plus or minus half the height of the collided object, linearly
                        // varying. We use a damping value so that the ball doesn't go too nuts.
                        this._speed.y = (this.position.y - collideInfo.other.position.y) * game.PADDLE_EDGESPEED_DAMPER;
                        // Since we're reflecting back, get the parent to tell the paddles to calculate where
                        // to go next. This doesn't happen for a vertical strike because there's no way to
                        // return it.
                        this._parent.reflect();
                    }
                    else {
                        // No, this is a collision coming off of the top or bottom of the paddle. In this
                        // case, just reflect our vertical direction instead, with no speed change.
                        //
                        // In this case the serve is not returnable because it's still moving left or right,
                        // but now it's going away from the paddle; so, don't tell the paddles to try and
                        // calculate a position here since it's pointless.
                        this._speed.flipY();
                    }
                    // This counts as a bounce of the ball; If the ball has bounced enough times, kick up the
                    // speed of the ball and change its color.
                    this._bounces++;
                    if (this._bounces % BALL_BOUNCE_TIMES == 0) {
                        // Speed up the ball.
                        this._speed.x *= BALL_BOUNCE_ACCELERATION;
                        // If the ball is not already at our last image, bump it up to the next one.
                        if (this._sprite < this._sheet.count - 1)
                            this._sprite++;
                    }
                }
                // Check if we are hitting the left or right side of the stage (or going off of it).
                //
                // When this occurs, someone is scoring points.
                if ((this._position.x >= stage.width && this._speed.x > 0) ||
                    (this._position.x <= 0 && this._speed.x < 0)) {
                    // Tell our parent that the ball is moving into a scoring position.
                    this._parent.score();
                }
                // Store the current ball position in the ball trail array. This needs to happen here and not
                // right after we move the ball, because intersections and bounces may cause the position to
                // be updated a bit.
                this.storeTrailPosition();
            };
            /**
             * Render the ball at the provided location and using the given renderer.
             *
             * We assume our position is at our center.
             *
             * @param x the x of the center of the ball
             * @param y the y of the center of the ball
             * @param renderer the renderer to blit with
             */
            Ball.prototype.render = function (x, y, renderer) {
                // If there is any stored trail information, render that first, back to front to get the
                // appropriate overlap.
                if (this._trail.length > 0) {
                    for (var arrPos = this._trailTailPos; arrPos != this._trailHeadPos; arrPos = (arrPos + 1) % BALL_TRAIL_MAXLEN) {
                        var pos = this._trail[arrPos];
                        this._sheet.blitCentered(this._sprite, pos.x, pos.y, renderer);
                    }
                }
                // Now render the ball at the location provided, which is our current location. We let the
                // super do that for us.
                _super.prototype.render.call(this, x, y, renderer);
            };
            return Ball;
        }(game.Entity));
        game.Ball = Ball;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The distance from the edge of the screen that each of the paddles is rendered. Larger values mean
         * farther from the edge of the screen and thus less reaction time.
         *
         * @type {number}
         */
        game.PADDLE_EDGESPACE = 50;
        /**
         * The speed that the keyboard moves the paddle if it's used as the control. THis applies to both the
         * left and right paddles equally/
         *
         * @type {number}
         */
        game.PADDLE_KEYBOARD_SPEED = 10;
        /**
         * The speed that the computer moves the paddle when it is in control (i.e. in a single player gme).
         * A higher value makes the paddle move faster, which makes the computer harder to beat.
         *
         * @type {number}
         */
        game.PADDLE_COMPUTER_SPEED = 10;
        /**
         * When a paddle is controlled via the computer and has calculated its final target position, the
         * target is modified by this value up or down from the calculated intersection point.
         *
         * This represents the percentage of the height of the paddle the target location can be skewed by at
         * most, either up or down.
         *
         * TODO This value could be modified to set difficulty
         *
         * @type {number}
         */
        game.PADDLE_MISS_FUDGE = 0.75;
        /**
         * This is used in the calculations that alter the vertical speed of the ball when interacting with a
         * paddle.
         *
         * The collision code calculates the difference in vertical position between the ball and the collided
         * object and uses that as a base speed, which is multiplied by this value to tone it down a little
         * bit so as to not allow the ball to get too wild.
         *
         * @type {number}
         */
        game.PADDLE_EDGESPEED_DAMPER = 0.40;
        /**
         * This sets the limit for how far away from the edges of the screen the vertical position of the
         * paddle can get before it is no longer allowed to move in that direction. A value of 0 means that
         * the center of the paddle will stop right at the extreme limits of the screen edges.
         *
         * The value here mimics older pong games that stop the paddles before they get to the edges, to
         * provide for a little extra "strategy".
         * @type {number}
         */
        game.PADDLE_EDGE_MOVE_LIMIT = 90;
        /**
         * The paddle entity is the entity that controls the paddle used by the player(s). Also included is an
         * AI module that will allow it to attempt to track the ball in order to return a serve.
         */
        var Paddle = (function (_super) {
            __extends(Paddle, _super);
            /**
             * Construct a new paddle handled by the provided stage. The paddle is always initially vertically
             * centered on the stage, but its horizontal position needs to be provided as it can be on either
             * the left or right side of the screen.
             *
             * The image string provided is the filename of the image to use as the sprite sheet for this
             * paddle. The sprite number provided will be the one used to render this paddle.
             *
             * @param stage the stage to render on
             * @param x initial x position of the center of the paddle
             * @param image the filename of the image to use to render this paddle; should be a sprite sheet image
             * @param sprite the sprite number in the sprite image to use for this paddle.
             */
            function Paddle(stage, x, image, sprite) {
                var _this = this;
                // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
                // of ourselves. We always start with our position centered vertically, but we need to be told
                // our horizontal position.
                _super.call(this, "paddle", stage, x, stage.height / 2, 0, 0, 1, {}, {}, 'red');
                /**
                 * Set the dimensions of the entity based on the size of the sprites in the SpriteSheet provided
                 *
                 * @param sheet the image that will represent us.
                 */
                this.setDimensions = function (sheet) {
                    // Set our dimensions and set the origin to be the center of the paddle.
                    _this._width = sheet.width;
                    _this._height = sheet.height;
                    _this._origin.setToXY(_this._width / 2, _this._height / 2);
                };
                // Create our sprite sheet for the paddle; setDimensions() will be called to finish our
                // initialization.
                this._sheet = new game.SpriteSheet(stage, image, 2, 1, true, this.setDimensions);
                this._sprite = sprite;
                // The target location defaults to our current location, so we don't move (if we're tracking
                // anything).
                this._targetY = this._position.y;
            }
            /**
             * Set the ball that this paddle should track, or (when ball is null) stop tracking and return
             * player control.
             *
             * When the paddle is tracking a ball, every frame update it will attempt to shift its direction
             * in order to intercept the ball at the location it expects it to cross the vertical axis of this
             * paddle.
             *
             * Invoking this method causes the paddle to immediately calculate and start moving towards its
             * desired location; once the paddle gets there it will stop moving. Invoke updateTracking() every
             * time the ball changes directions in order to allow the paddle to update itself accordingly.
             *
             * @param ball the ball that we should track
             * @see updateTracking
             */
            Paddle.prototype.track = function (ball) {
                this._ball = ball;
                this.updateTracking();
            };
            /**
             * Every time this is invoked, the paddle will automatically recalculate where it should be moving
             * to based on the current ball location and direction.
             *
             * This is implicitly invoked every time the paddle is told to track a ball. However, the paddle
             * only updates its position based on the current direction of the ball (left or right); every
             * time the ball changes its direction, you should invoke this to tell the paddle so that it knows
             * what to do next.
             *
             * @see track
             */
            Paddle.prototype.updateTracking = function () {
                // If we are tracking a ball, calculate a new position.
                if (this._ball != null)
                    this.updateTargetPosition();
            };
            /**
             * This analyzes the current ball location in relation to the paddle to determine where exactly we
             * should attempt to move the paddle to so that we will return the shot.
             *
             * This calculates the trajectory of the ball where it will cross our location by determining
             * where any interim bounces will be.
             */
            Paddle.prototype.updateTargetPosition = function () {
                // If the ball is moving away from us, we set our target position to the center of the stage
                // so that we are better positioned to return the ball.
                //
                // In order to determine this we check the sign of the X velocity of the ball and (crudely)
                // check what half of the screen we're on to know if we care about left or right.
                //
                // This last bit is important because based on how our collision detection works, the ball
                // might be slightly behind our actual X position, in which case we will miss that it's going
                // away from us because it appears to be coming towards us.
                // If the ball is moving away from us, our target position should be the center of the stage,
                // so that we are positioned as best as possible to hit the next shot.
                if ((this._position.x <= this._stage.width / 2 && this._ball.velX > 0) ||
                    (this._position.x > this._stage.width / 2 && this._ball.velX < 0)) {
                    this._targetY = this._stage.height / 2;
                }
                else {
                    // The ball must be moving towards us. In that case, we should attempt to determine the
                    // location where the ball will eventually cross our side of the screen, so that we can move
                    // there to return the ball.  This takes into account the ball will take even if the return
                    // is going to bounce one or more times before it eventually gets to us.  Determine the X
                    // coordinate of the computer paddle. This is always the same value, but putting it in a
                    // variable makes the code less ugly below.
                    var compPaddlePos = this._position.x;
                    // The trajectory of the ball is just a regular line. The equation for a line is:
                    //     y = mx + b
                    //
                    // Where m is the slope of the line (rise over run or change in y over change in x) and b is
                    // the point at which the line intercepts the Y axis.  In order to determine where the ball
                    // is going to intersect, we first need to determine the slope of the line.
                    var slope = this._ball.velY / this._ball.velX;
                    // The trajectory of the ball is a line and the point that the ball is on is on it. However,
                    // since the line might reflect off of the top and bottom of the screen, the positions of the
                    // ball also change. So we have temporary copies here. For the calculations below, we need an
                    // X point to use. We will start with the location of the ball.
                    var tmpBallX = this._ball.position.x;
                    var tmpBallY = this._ball.position.y;
                    // We need to know the y axis intercept of the ball trajectory; however, this will change
                    // depending on the direction that the ball is going, and the ball will change directions if
                    // it reflects off of the top or bottom of the screen.  So, we keep looping trying to
                    // calculate the eventual position of the ball until it is in range.
                    do {
                        // Calculate b, the Y axis intercept. If we rearrange the equation of the line a little
                        // bit, we end up with:  b = y - mx  We know the slope and the location of the ball, so
                        // we can calculate the intercept. Note that the intercept changes when the slope changes
                        // from negative to positive, which is why this is inside the loop.
                        var yIntercept = tmpBallY - (slope * tmpBallX);
                        // Now calculate what the Y position will be when the ball reaches the paddle location.
                        this._targetY = (slope * compPaddlePos) + yIntercept;
                        // The target value will be out of range of the canvas height if the trajectory of the
                        // ball is steep enough to bounce off of the top or the bottom. If that happens, then we
                        // need to loop again.
                        if (this._targetY < 0 || this._targetY >= this._stage.height) {
                            // First, set the new ball location to be either the top or bottom of the screen.
                            if (this._targetY < 0)
                                tmpBallY = 0;
                            else
                                tmpBallY = this._stage.height - 1;
                            // Now, using the slope and intercept that we have, insert that Y value to determine
                            // where the X value of the bounce was.  The equation is reworked as:  x = (y - b) /
                            // x
                            tmpBallX = (tmpBallY - yIntercept) / slope;
                            // Reflect the slope of the trajectory to account for the bounce.
                            slope *= -1;
                        }
                    } while (this._targetY < 0 || this._targetY >= this._stage.height);
                }
                // At this point, we know exactly where the ball will cross the paddle line. However, going
                // straight there is bad for a few reasons:
                //   1) The computer is pretty much unbeatable because it plays too perfect.
                //   2) The Y trajectory of the ball reflection with the paddle is governed by where on the paddle
                //      the ball hits. The center means a straight line out, which is boring.
                //
                // For these reasons (at least), we perturb the actual computed Y location to target by adding a
                // random factor to the final location, either up or down (clipping as needed). This will make the
                // computer sometimes miss the ball and when it does hit, it will bounce more unpredictably.
                //
                // This also has the effect of masking how perfectly the paddle zeroes in on the center of the
                // field after it returns the ball.
                this._targetY += game.Utils.randomIntInRange(-(this._height * game.PADDLE_MISS_FUDGE), (this._height * game.PADDLE_MISS_FUDGE));
                if (this._targetY < 0)
                    this._targetY = 0;
                else if (this._targetY >= this._stage.height)
                    this._targetY = this._stage.height - 1;
            };
            /**
             * This is invoked every frame to update this paddle.
             *
             * If we are currently tracking a ball, then this method will use the current location and
             * velocity of the ball to attempt to intercept the ball. Otherwise this does nothing.
             *
             * @param stage the stage the paddle is on
             * @param tick the game tick, for timing purposes
             */
            Paddle.prototype.update = function (stage, tick) {
                // If we are not tracking a ball, do nothing; a human is controlling this paddle.
                if (this._ball == null)
                    return;
                // Try to move in the direction of our calculated intercept location. We will only actually
                // move if we're not in any danger of going past our target. This adds a little extra chance
                // to how the ball will react when (if) it hits us.
                if (this._position.y < this._targetY - game.PADDLE_COMPUTER_SPEED)
                    this.move(game.PADDLE_COMPUTER_SPEED);
                else if (this._position.y > this._targetY + game.PADDLE_COMPUTER_SPEED)
                    this.move(-game.PADDLE_COMPUTER_SPEED);
            };
            /**
             * Jump this paddle directly so that its vertical position is the position passed in. This does
             * limit checking to ensure the position is set to be as valid as possible.
             *
             * @param newPos the new Y position.
             */
            Paddle.prototype.jumpTo = function (newPos) {
                // Set the current Y position and make sure that it is clamped to an acceptable range.
                this._position.y = game.Utils.clampToRange(newPos, game.PADDLE_EDGE_MOVE_LIMIT, this._stage.height - game.PADDLE_EDGE_MOVE_LIMIT);
            };
            /**
             * Shift this paddle up or down on the screen by the provided amount.
             *
             * Bounds checks are done to ensure that the paddle stops half way off the top or bottom of the stage.
             *
             * @param amount the amount to move (positive for down, negative for up).
             */
            Paddle.prototype.move = function (amount) {
                // Adjust the current position by the provided amount and make sure that it is clamped to an
                // acceptable range.
                this.jumpTo(this._position.y + amount);
            };
            return Paddle;
        }(game.Entity));
        game.Paddle = Paddle;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * If the game remains on the title screen for this number of consecutive ticks, the game is
         * automatically kicked off with 0 players to start "attract mode".
         *
         * There are 30 ticks in a second because we run at 30fps, so there are 30 * 60 ticks per minute.
         *
         * @type {number}
         */
        game.ATTRACT_MODE_TICKS = (30 * 60) * 2;
        /**
         * This scene represents the title screen, where the game is described and the number of players is
         * selected.
         */
        var TitleScene = (function (_super) {
            __extends(TitleScene, _super);
            /**
             * Construct a new title screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             * @param music the music to play (while music is not muted)
             */
            function TitleScene(stage, music) {
                _super.call(this, "titleScreen", stage);
                /**
                 * This is true until the first time the scene is activated, then it becomes false; we use this to
                 * determine if we should start our music playing or not.
                 *
                 * @type {boolean}
                 * @private
                 */
                this._firstActivation = true;
                // Save the music.
                this._music = music;
                // Indicate that we want to load a background image. This is implicitly shared if another
                // scene loads the same image because image preloads are shared (music is not).
                this._background = stage.preloadImage("background.png");
            }
            /**
             * This gets invoked when our scene becomes the active scene; If the music is not muted and not
             * already playing, this starts it.
             *
             * @param previousScene the scene that used to be active.
             */
            TitleScene.prototype.activating = function (previousScene) {
                // Let the super do its thing.
                _super.prototype.activating.call(this, previousScene);
                // Reset our attract mode counter.
                this._attractTicks = 0;
                // If we have music and this is the first time we've been activated, start the music playing;
                // we don't want to play the music at other times in case it's not playing because the user
                // muted it.
                if (this._music && this._firstActivation)
                    this._music.play(false);
                // Clear the flag now
                this._firstActivation = false;
            };
            /**
             * Invoked every time a key is pressed on the title screen.
             *
             * @param eventObj the keyboard event that says what key was pressed
             * @returns {boolean} true if we handle the key, false otherwise.
             */
            TitleScene.prototype.inputKeyDown = function (eventObj) {
                // If the super handles the key, return true. This lets default handling for known keys do its
                // thing.
                if (_super.prototype.inputKeyDown.call(this, eventObj))
                    return true;
                switch (eventObj.keyCode) {
                    // Toggle the mute state of the music
                    case game.KeyCodes.KEY_SPACEBAR:
                    case game.KeyCodes.KEY_M:
                        if (this._music)
                            this._music.toggle(false);
                        return true;
                    // Select a single or two player game; you can also force attract mode by pressing 0,
                    // although this is not documented. Easter Egg!
                    case game.KeyCodes.KEY_0:
                    case game.KeyCodes.KEY_1:
                    case game.KeyCodes.KEY_2:
                        game.playerCount = eventObj.keyCode - game.KeyCodes.KEY_0;
                        this._stage.switchToScene("game");
                        return true;
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                }
                return false;
            };
            /**
             * Invoked every update.
             * @param tick
             */
            TitleScene.prototype.update = function (tick) {
                // Let the super do its thing.
                _super.prototype.update.call(this, tick);
                // Update our tick count internally. If it reaches the appropriate point, start the game with
                // 0 players.
                this._attractTicks++;
                if (this._attractTicks >= game.ATTRACT_MODE_TICKS) {
                    game.playerCount = 0;
                    this._stage.switchToScene("game");
                }
            };
            /**
             * This is called to invoke our
             */
            TitleScene.prototype.render = function () {
                // Render our background. This should cover the whole canvas, so there is no need to do any
                // clearing.
                this._renderer.blit(this._background, 0, 0);
                // Save the current context state.
                this._renderer.translateAndRotate(null, null, null);
                // Title text.
                this._renderer.context.font = "64px monospace";
                this._renderer.context.textAlign = "center";
                this._renderer.drawTxt("Tennis Mania", this._renderer.width / 2, 64, 'green');
                // Rule text.
                this._renderer.context.font = "25px monospace";
                this._renderer.context.textAlign = "left";
                this._renderer.drawTxt("First one to " + game.WINNING_SCORE + " points wins!", 20, 150, 'white');
                this._renderer.drawTxt("The other player is the big L.O.S.E.R!", 35, 180, 'red');
                // Controls
                this._renderer.drawTxt("Single player controls:", 20, 250, 'white');
                this._renderer.drawTxt("mouse or the 'w' and 's' keys", 80, 280, 'darkred');
                this._renderer.drawTxt("Two player controls:", 20, 350, 'white');
                this._renderer.drawTxt("player 1 -> 'w' and 's' keys", 80, 380, 'darkred');
                this._renderer.drawTxt("player 2 -> arrow keys", 80, 410, 'darkred');
                this._renderer.drawTxt("Press 'm' or space bar to toggle music", 20, 480, 'darkred');
                this._renderer.drawTxt("Press 'f' to toggle full screen", 20, 510, 'darkred');
                // Player select instructions
                this._renderer.context.textAlign = 'center';
                this._renderer.drawTxt("Press '1' or '2' to select the number of players", this._renderer.width / 2, this._renderer.height - 25, 'white');
                // Restore the context and let the super render things now.
                this._renderer.restore();
                _super.prototype.render.call(this);
            };
            return TitleScene;
        }(game.Scene));
        game.TitleScene = TitleScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * This scene represents the game screen, where the game is actually played.
         */
        var GameScene = (function (_super) {
            __extends(GameScene, _super);
            /**
             * Construct a new game screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             * @param music the music to play (while music is not muted)
             */
            function GameScene(stage, music) {
                _super.call(this, "gameScreen", stage);
                /**
                 * The list of colors that we use to display our attract mode text if we are in attract mode (0
                 * players).
                 * @type {Array<string>}
                 * @private
                 */
                this._attractColors = ['#ffffff', '#000000'];
                /**
                 * The index into _attractColors that we use to display attract mode text (if we're doing that).
                 *
                 * @type {number}
                 * @private
                 */
                this._colorIndex = 0;
                // Save the music.
                this._music = music;
                // Preload all of the sounds we use. We load our own sounds; the music is given to us because
                // its shared amongst all scenes.
                this._sndScore = stage.preloadSound("score");
                this._sndBouncePaddle = stage.preloadSound("bounce_paddle");
                this._sndBounceWall = stage.preloadSound("bounce_wall");
                // Indicate that we want to load a background image. This is implicitly shared if another
                // scene loads the same image because image preloads are shared (music is not).
                this._background = stage.preloadImage("background.png");
                // Create our entities now.
                this._ball = new game.Ball(stage, this);
                this._paddleLeft = new game.Paddle(stage, game.PADDLE_EDGESPACE, "paddle_2_1.png", 0);
                this._paddleRight = new game.Paddle(stage, stage.width - game.PADDLE_EDGESPACE, "paddle_2_1.png", 1);
                // // Turn on debug mode for all of the entities so that we can verify that everything is working
                // // the way we want it to.
                // this._ball.properties.debug = true;
                // this._paddleLeft.properties.debug = true;
                // this._paddleRight.properties.debug = true;
                // Register the paddle objects with the ball; these are the objects that it can collide with
                // to change directions.
                this._ball.addCollider(this._paddleLeft);
                this._ball.addCollider(this._paddleRight);
                // Add all entities as actors so that they get updated and rendered. Note that we add the ball
                // first; that means it gets updates and renders first, which means that it renders below the
                // paddles, which is a nice visual tweak to hide our less than perfect collision routines.
                this.addActor(this._ball);
                this.addActor(this._paddleLeft);
                this.addActor(this._paddleRight);
                // By default there is no motion.
                this._leftControl = 0;
                this._rightControl = 0;
            }
            /**
             * This gets invoked when our scene becomes the active scene; If the music is not muted and not
             * already playing, this starts it.
             *
             * @param previousScene the scene that used to be active.
             */
            GameScene.prototype.activating = function (previousScene) {
                // Let the super do its thing.
                _super.prototype.activating.call(this, previousScene);
                // This really only has to happen once, but make sure that we know the sizes of the paddles.
                game.setPaddleWidths(this._stage, this._paddleLeft.width, this._paddleRight.width);
                // Reset the scores back to zero, and get the ball ready.
                game.resetScores();
                this._ball.reset();
                // We are becoming the active scene; Tell our paddles to track the ball (or not) depending on
                // the number of players. The left paddle only tracks the ball when nobody is playing, and the
                // right paddle only tracks the ball when a second person is not playing.
                this._paddleLeft.track(game.playerCount == 0 ? this._ball : null);
                this._paddleRight.track(game.playerCount != 2 ? this._ball : null);
            };
            /**
             * This checks the key code provided to if it is one of the keys that handles controlling the
             * paddles in the game, and takes appropriate action.
             *
             * The boolean parameter pressed tells us if the button is currently pressed (true) or released
             * (false), so that we know how to move the appropriate paddle.
             *
             * This takes care to make sure that two player controls control the first player when in single
             * player mode.
             *
             * @param eventObj the event object that
             * @param pressed true if the event indicates the button is pressed or false if it was released
             * @returns {boolean} true if the event was handled or false otherwise.
             */
            GameScene.prototype.handlePaddleKey = function (eventObj, pressed) {
                // Handle based on the key code.
                switch (eventObj.keyCode) {
                    // Right player controls move the right paddle in a two player game and the left paddle in a
                    // single player game.
                    case game.KeyCodes.KEY_UP:
                        if (game.playerCount == 1)
                            this._leftControl = (pressed ? -1 : 0);
                        else if (game.playerCount == 2)
                            this._rightControl = (pressed ? -1 : 0);
                        return true;
                    case game.KeyCodes.KEY_DOWN:
                        if (game.playerCount == 1)
                            this._leftControl = (pressed ? 1 : 0);
                        else if (game.playerCount == 2)
                            this._rightControl = (pressed ? 1 : 0);
                        return true;
                    // Left player keys only ever control the left paddle no matter what, if there is at least
                    // one player.
                    case game.KeyCodes.KEY_W:
                        if (game.playerCount != 0)
                            this._leftControl = (pressed ? -1 : 0);
                        return true;
                    case game.KeyCodes.KEY_S:
                        if (game.playerCount != 0)
                            this._leftControl = (pressed ? 1 : 0);
                        return true;
                    // We don't handle anything else.
                    default:
                        return false;
                }
            };
            /**
             * Invoked every time a key is pressed on the game screen.
             *
             * @param eventObj the keyboard event that says what key was pressed
             * @returns {boolean} true if we handle the key, false otherwise.
             */
            GameScene.prototype.inputKeyDown = function (eventObj) {
                // If the number of players is 0, ignore everything else and immediately switch back to the
                // title screen scene.
                if (game.playerCount == 0) {
                    this._stage.switchToScene("title");
                    return true;
                }
                // If the super handles the key or if it is a paddle key, return true.
                if (_super.prototype.inputKeyDown.call(this, eventObj) || this.handlePaddleKey(eventObj, true))
                    return true;
                // See if it's something else we care about.
                switch (eventObj.keyCode) {
                    // Toggle the mute state of the music
                    case game.KeyCodes.KEY_SPACEBAR:
                    case game.KeyCodes.KEY_M:
                        if (this._music)
                            this._music.toggle(false);
                        return true;
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                }
                return false;
            };
            /**
             * Invoked every time a key is released on the game screen.
             *
             * @param eventObj the keyboard event that says what key was released
             * @returns {boolean} true if we handle the key, false otherwise.
             */
            GameScene.prototype.inputKeyUp = function (eventObj) {
                // Check if this is a paddle key being released.
                if (this.handlePaddleKey(eventObj, false))
                    return true;
                // Let the super do its thing.
                return _super.prototype.inputKeyUp.call(this, eventObj);
            };
            /**
             * Handle a mouse click on the canvas while the game is in play.
             *
             * @param eventObj the mouse event that represents the click
             * @returns {boolean} true if we handle the click, false otherwise.
             */
            GameScene.prototype.inputMouseClick = function (eventObj) {
                // If this is a 0 player game, switch back to the title screen.
                if (game.playerCount == 0) {
                    this._stage.switchToScene("title");
                    return true;
                }
                // Do what the super does.
                return _super.prototype.inputMouseClick.call(this, eventObj);
            };
            /**
             * This is triggered whenever the mouse is moved over the canvas. In a single player game, this
             * controls where the left paddle is located.
             *
             * @param eventObj the event that represents the mouse movement.
             * @returns {boolean} true if we handled this event or false if not.
             */
            GameScene.prototype.inputMouseMove = function (eventObj) {
                // If we're in single player mode, we want to read the mouse position and use the vertical
                // component to set the position of the left paddle.
                if (game.playerCount == 1) {
                    // Get the mouse position and use it to set the vertical position of the left paddle.
                    this._paddleLeft.jumpTo(this._stage.calculateMousePos(eventObj).y);
                    return true;
                }
                return false;
            };
            /**
             * This helper tells both of the paddles to update their tracking position based on the current
             * location and momentum of the ball.
             *
             * Any paddle that is not currently tracking the ball will ignore this call.
             */
            GameScene.prototype.updatePaddleTracking = function () {
                this._paddleLeft.updateTracking();
                this._paddleRight.updateTracking();
            };
            /**
             * This gets invoked every time the ball gets bounced off of a wall in the game in a non-scoring
             * situation.
             */
            GameScene.prototype.wallBounce = function () {
                // Play the sound for the ball bouncing off of the wall.
                this._sndBounceWall.play();
            };
            /**
             * This gets invoked every time the ball gets reflected from one side of the screen to the other
             * by bouncing off a paddle (or other collider).
             *
             * This happens after the reflection has happened.
             */
            GameScene.prototype.reflect = function () {
                // Play the paddle collision sound.
                this._sndBouncePaddle.play();
                // Tell the paddles that the ball has reflected so that they can update their positions.
                this.updatePaddleTracking();
            };
            /**
             * This gets invoked every time the ball reaches the right or left side of the screen.
             *
             * The ball invokes this when it discovers that it has gone off the screen, allowing us to handle
             * it as appropriate.
             */
            GameScene.prototype.score = function () {
                // Determine the score that will end the game.
                var winningScore = (game.playerCount == 0 ? game.ATTRACT_SCORE : game.WINNING_SCORE);
                // Update the score of the appropriate player based on the position of the ball. This has to
                // be first, since we're about to fiddle the ball position.
                if (this._ball.position.x <= 0)
                    game.player2Score++;
                else
                    game.player1Score++;
                // If either score is a winning score, switch to the appropriate scene.
                //
                // In "Attract mode" (0 players) we go back to the title screen instead of saying the score at
                // the win screen.
                if (game.player1Score >= winningScore || game.player2Score >= winningScore)
                    this._stage.switchToScene(game.playerCount != 0 ? "win" : "title");
                // Play the score sound
                this._sndScore.play();
                // Reset the position of the ball and then get both of the paddles to update their tracking
                // based on the new position.
                this._ball.reset();
                this.updatePaddleTracking();
            };
            /**
             * Invoked once per game tick to update the frame and its contents.
             *
             * @param tick the current tick; increases by one for each invocation
             */
            GameScene.prototype.update = function (tick) {
                // Update the attract mode text color.
                if (game.playerCount == 0 && tick % 7 == 0) {
                    this._colorIndex++;
                    if (this._colorIndex == this._attractColors.length)
                        this._colorIndex = 0;
                }
                // Use the control values to shift the locations of the paddles. This happens in response to
                // the keyboard controls; the mouse
                if (this._leftControl != 0)
                    this._paddleLeft.move(this._leftControl * game.PADDLE_KEYBOARD_SPEED);
                if (this._rightControl != 0)
                    this._paddleRight.move(this._rightControl * game.PADDLE_KEYBOARD_SPEED);
                // let the super update; this calls update on our entities, which means that the ball will
                // move and the computer controlled paddle will have a chance to move itself.
                _super.prototype.update.call(this, tick);
            };
            /**
             * This renders some attract mode text; it should really be invoked only when
             */
            GameScene.prototype.renderAttractMode = function () {
                // Save the current render context so we can fiddle with it
                this._renderer.translateAndRotate(null, null, null);
                // Set up our font and render some text.
                this._renderer.context.font = "25px monospace";
                this._renderer.context.textAlign = "center";
                this._renderer.context.textBaseline = "middle";
                this._renderer.drawTxt("Press any key to play!", this._stage.width / 2, this._stage.height / 2, this._attractColors[this._colorIndex]);
                // Restore the context now
                this._renderer.restore();
            };
            /**
             * This is called whenever our scene needs to be rendered.
             */
            GameScene.prototype.render = function () {
                // Render our background. This should cover the whole canvas, so there is no need to do any
                // clearing.
                this._renderer.blit(this._background, 0, 0);
                // Let the super render things now. This will render the paddle and the ball for us.
                _super.prototype.render.call(this);
                // If there are 0 players, render our attract mode text.
                if (game.playerCount == 0)
                    this.renderAttractMode();
                // Now render the scores last, so that they appear on top of everything else.
                game.renderScores(this._renderer);
            };
            return GameScene;
        }(game.Scene));
        game.GameScene = GameScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * This is the scene that is displayed when someone wins the game. This can be either the/a player or
         * the computer, depending on what kind of game we have.
         */
        var WinScene = (function (_super) {
            __extends(WinScene, _super);
            /**
             * Construct a new win screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             * @param music the music to play (while music is not muted)
             */
            function WinScene(stage, music) {
                _super.call(this, "winScreen", stage);
                // Save the music.
                this._music = music;
                // Indicate that we want to load a background image. This is implicitly shared if another
                // scene loads the same image because image preloads are shared (music is not).
                this._background = stage.preloadImage("background.png");
                // Save the center of the stage.
                this._center = new game.Point(stage.width / 2, stage.height / 2);
            }
            /**
             * Invoked every time a key is pressed on the win screen.
             *
             * @param eventObj the keyboard event that says what key was pressed
             * @returns {boolean} true if we handle the key, false otherwise.
             */
            WinScene.prototype.inputKeyDown = function (eventObj) {
                // If the super handles the key, return true. This lets default handling for known keys do its
                // thing.
                if (_super.prototype.inputKeyDown.call(this, eventObj))
                    return true;
                switch (eventObj.keyCode) {
                    // Toggle the mute state of the music
                    case game.KeyCodes.KEY_SPACEBAR:
                    case game.KeyCodes.KEY_M:
                        if (this._music)
                            this._music.toggle(false);
                        return true;
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                    // Everything else starts a new game by going to the title screen.
                    default:
                        this._stage.switchToScene("title");
                        return true;
                }
            };
            /**
             * Invoked every time the mouse is clicked on the win screen.
             *
             * @param eventObj the mouse event that gives click details
             * @returns {boolean} true if we handle the click, false otherwise.
             */
            WinScene.prototype.inputMouseClick = function (eventObj) {
                // Start a new game by going to the title screen
                this._stage.switchToScene("title");
                return true;
            };
            /**
             * This is called to invoke our
             */
            WinScene.prototype.render = function () {
                // Render our background. This should cover the whole canvas, so there is no need to do any
                // clearing.
                this._renderer.blit(this._background, 0, 0);
                // Now the game scores.
                game.renderScores(this._renderer);
                // Determine the message that we will display. This depends on who won and how many players
                // there are.
                var message;
                if (game.playerCount == 1)
                    message = (game.player1Score >= game.WINNING_SCORE ? "You Win!" : "Computer Wins!");
                else
                    message = (game.player1Score >= game.WINNING_SCORE ? "Player 1 wins!" : "Player 2 wins!");
                // Save the current context state.
                this._renderer.translateAndRotate(null, null, null);
                // Win message
                this._renderer.context.font = "64px monospace";
                this._renderer.context.textAlign = "center";
                this._renderer.drawTxt(message, this._center.x, this._center.y, 'white');
                // The message for how to restart the game
                this._renderer.context.font = "25px monospace";
                this._renderer.drawTxt("Click or press any key to play again", this._center.x, this._center.y + 80, 'white');
                // Restore the context and let the super render things now.
                this._renderer.restore();
                _super.prototype.render.call(this);
            };
            return WinScene;
        }(game.Scene));
        game.WinScene = WinScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var main;
    (function (main) {
        /**
         * Set up the button on the page to toggle the state of the game.
         *
         * @param stage the stage to control
         * @param buttonID the ID of the button to mark up to control the game state
         */
        function setupButton(stage, buttonID) {
            // True when the game is running, false when it is not. This state is toggled by the button. We
            // assume that the game is going to start running.
            var gameRunning = true;
            // Get the button.
            var button = document.getElementById(buttonID);
            if (button == null)
                throw new ReferenceError("No button found with ID '" + buttonID + "'");
            // Set up the button to toggle the stage.
            button.addEventListener("click", function () {
                // Try to toggle the game state. This will only throw an error if we try to put the game into
                // a state it is already in, which can only happen if the engine stops itself when we didn't
                // expect it.
                try {
                    if (gameRunning) {
                        stage.muteMusic(true);
                        stage.muteSounds(true);
                        stage.stop();
                    }
                    else {
                        stage.muteMusic(false);
                        stage.muteSounds(false);
                        stage.run();
                    }
                }
                // Log and then rethrow the error.
                catch (error) {
                    console.log("Exception generated while toggling game state");
                    throw error;
                }
                finally {
                    // No matter what, toggle the state.
                    gameRunning = !gameRunning;
                    button.innerHTML = gameRunning ? "Stop Game" : "Restart Game";
                }
            });
        }
        // Once the DOM is loaded, set things up.
        nurdz.contentLoaded(window, function () {
            try {
                // Set up the stage.
                var stage = new nurdz.game.Stage('gameContent', 'black', true, '#a0a0a0');
                // Set up the default values used for creating a screen shot.
                nurdz.game.Stage.screenshotFilenameBase = "ts-tennis";
                nurdz.game.Stage.screenshotWindowTitle = "ts-tennis";
                // Set up the button that will stop the game if something goes wrong.
                setupButton(stage, "controlBtn");
                // The music that will play in all scenes.
                var music = stage.preloadMusic("ElectroCabello");
                // Register all of our scenes.
                stage.addScene("title", new nurdz.game.TitleScene(stage, music));
                stage.addScene("game", new nurdz.game.GameScene(stage, music));
                stage.addScene("win", new nurdz.game.WinScene(stage, music));
                // Switch to the initial scene, add a dot to display and then run the game.
                stage.switchToScene("title");
                stage.run();
            }
            catch (error) {
                console.log("Error starting the game");
                throw error;
            }
        });
    })(main = nurdz.main || (nurdz.main = {}));
})(nurdz || (nurdz = {}));
