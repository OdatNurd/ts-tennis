This is a simplistic pong clone written in TypeScript.

The inspiration for this particular course is from the [udemy](http://udemmy.com)
course [How to Program Games](https://www.udemy.com/how-to-program-games/) by
[Chris DeLeon](https://twitter.com/ChrisDeLeon).

In that course, Chris teaches how to create video games by recreating classic
games in HTML5/JavaScript. The course also comes with a textbook with more
"exercises" in it that get you to extend your skills by adding to the game
as laid out in the course.

The code in this repository is a port of the Tennis game from the course from
JavaScript to TypeScript using the engine I created during [Devember 2015](http://devember.org),
which you can find the source for on [GitLab](https://gitlab.com) at
<https://gitlab.com/OdatNurd/ts-game-engine>.

As mentioned, this code goes past the code presented in the course, as I work
through all of the exercises associated with the tennis game.

Note that instead of forking the actual engine project, here I have worked from
that as a base to come up with this project. The main difference is that there
is no source for the engine here; rather, just the TypeScript definitions
and compiled JavaScript output.
